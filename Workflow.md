# Workflow 

- Create an Issue everytime work has to be done
- Create a merge request whenever someone works on the issue, assign yourself to the issue, estimate time management
- Commit changes and submit ready when done, track your time
- Project leader reviews Code and discusses with the dev
- Dev corrects problems
- Project leader merges code