# Bienvenu sur le repository de Costory :book:

<div align="center">
  <img src="src/frontend/app/src/assets/img/logo.png" width="250" title="Costory_logo">
</div>


Costory est un site permettant à des utilisateurs de créer ensemble des histoires !

Notre site est accessible à l'adresse : [costory.ch](http://costory.ch) 

Pour en apprendre plus sur notre projet, visitez notre [wiki](https://gitlab.com/corona-digital/costory/-/wikis/home)

# Vidéo

Notre vidéo : <a href="https://youtu.be/HBNm6-By7po">https://youtu.be/HBNm6-By7po</a>

# Equipe

- Chau Ying Kot (@KurohanaJuri): UI/UX Designer (chau.kot@heig-vd.ch)
- Bastien Potet (@BPotet): UI/UX Designer (bastien.potet@heig-vd.ch)
- Simon Mattei (@Ikewolf77): Backend & Déploiement (simon.mattei@heig-vd.ch)
- Maurice Lehmann (@mauricelehmann) : Chef de projet / Backend & Déploiement (maurice.lehmann@heig-vd.ch)

Vous voulez contribuer? Venez [ici!](https://gitlab.com/corona-digital/costory/-/wikis/Comment-contribuer)
