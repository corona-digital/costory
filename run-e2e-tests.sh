#!/bin/bash

cd src

# Remove old report if exist
if [ -d frontend/e2e/cypress/reports ]; then
  rm -rf frontend/e2e/cypress/reports
fi

tag=local docker-compose -f docker-compose.yml -f docker-compose.test.yml -f docker-compose.local.yml build db frontend cypress
tag=local docker-compose -f docker-compose.yml -f docker-compose.local.yml build backend
sh ./run-e2e-with-tag.sh local


# Sleep the script for waiting the launch of the database
while [ ! -d frontend/e2e/cypress/reports ];
do
  sleep 2
done

# Clean our tests

touch frontend/e2e/cypress/reports/.gitkeep

docker-compose down
