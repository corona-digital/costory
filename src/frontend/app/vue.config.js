module.exports = {
  devServer: {
    proxy: {
      '/api': {
        target: process.env.VUE_APP_API_URL_SERVER,
        changeOrigin: true,
        pathRewrite: {
          '^/api/': ''
        }
      }
    }
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `
            @import "@/assets/styles/costory.scss";
        `
      }
    }
  }
}
