module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  transform: {
    '^.+\\.vue$': 'vue-jest'
  },
  reporters: [
    'default',
    ['jest-junit', {
      outputDirectory: './reports'
    }]
  ]
}
