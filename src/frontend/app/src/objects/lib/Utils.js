import moment from 'moment'
import { StoryBuilder } from '@/objects/story/Story'
import { TagBuilder } from '@/objects/tag/Tag'

const urlApi = process.env.VUE_APP_API_URL

/**
 * @param value{string} the value to check if it's a number
 * @returns {boolean} true if value is a number else false
 */
function isNumber (value) {
  return typeof value === 'number'
}

/**
 * Custom format for date
 * @param date
 * @returns {string} return the date with DD.MM.YYYY format
 */
function formatDate (date) {
  if (date) {
    return moment(String(date)).format('DD.MM.YYYY HH:mm')
  }
}

/**
 * @param value{string}
 * @returns {string} return the string concatenated 'value' with the urlApi
 */
function apiUrl (value) {
  return urlApi + value
}

/**
 * Crete a new Story without paragraph and comments with the data passed in the param
 * @param data Data from api
 * @returns {Story} return the story object
 */
function createStoryFromApi (data) {
  let nbCharacters = 0

  const tags = []

  for (const tag of data.tags) {
    tags.push(
      new TagBuilder()
        .setId(tag.id)
        .setName(tag.name)
        .build()
    )
  }

  for (const paragraph of data.paragraphs) {
    nbCharacters += paragraph.text.length
  }

  return new StoryBuilder()
    .setId(data.id)
    .setTitle(data.title)
    .setExcerpt(data.paragraphs[0].text)
    .setNbMinParagraphs(data.minParagraph)
    .setNbMaxParagraphs(data.maxParagraph)
    .setNbCharacters(nbCharacters)
    .setNbParagraphsAlreadyWritten(data.paragraphs.length)
    .setTags(tags)
    .setDone(data.isDone)
    .build()
}

export { isNumber, apiUrl, formatDate, createStoryFromApi }
