/**
 * Check it the elem passed is egal to the valueToCheck
 * if is true, the elem.success is set at true
 * otherwise, elem.errorDontMatch is set at true
 * @param elem {{value: *, errorDontMatch: boolean, success:boolean}} Element to test
 * @param valueToCheck{*} expected value
 */
function validateMatches (elem, valueToCheck) {
  if (elem.success) {
    elem.errorDontMatch = false
    elem.success = false

    if (elem.value === valueToCheck) {
      elem.success = true
    } else {
      elem.errorDontMatch = true
    }
  }
}

/**
 * Check if the elem passed in the parameter is valid (string not empty)
 * if is true, the elem.success is set at true
 * otherwise, elem.errorRequired is set at true
 * @param elem {{value: *, errorRequired: boolean, success:boolean}} Element to test
 */
function validateRequired (elem) {
  elem.errorRequired = false
  elem.success = false

  if (elem.value === '') {
    elem.errorRequired = true
  } else {
    elem.success = true
  }
}

/**
 * Check if the elem passed in the parameter contains less char than max
 * if is true, the elem.success is set at true
 * otherwise, elem.errorRequired is set at true
 * @param elem{{value:string, errorRequired:boolean, success:boolean}} Element to test
 * @param max{number} Maximum character allowed
 */
function checkMaxChar (elem, max) {
  elem.errorRequired = false
  elem.success = false

  if (elem.value === '' || elem.value.length > max) {
    elem.errorRequired = true
  } else {
    elem.success = true
  }
}

/**
 * Check if the range of values in the parameter are valid
 * if is true, the elem.success is set at true
 * otherwise, elem.errorRequired is set at true
 * @param value{{min: Number, max:Number, errorRequired: boolean, success:boolean}} Values to check
 */
function checkParagraphValue (value) {
  value.errorRequired = false
  value.success = false

  if (value.min <= 0 || value.max <= 0 || value.min > value.max || value.max < value.min) {
    value.errorRequired = true
  } else {
    value.success = true
  }
}

/**
 * Check if the tags.value contains at least one tag
 * if is true, the elem.success is set at true
 * otherwise, elem.errorRequired is set at true
 * @param tags{{value:number[], max:Number, errorRequired: boolean, success:boolean}}
 */
function hasAtLeastOneTag (tags) {
  tags.errorRequired = false
  tags.success = false

  if (tags.value.length < 1) {
    tags.errorRequired = true
  } else {
    tags.success = true
  }
}

export { validateMatches, validateRequired, checkMaxChar, checkParagraphValue, hasAtLeastOneTag }
