import { isNumber } from '@/objects/lib/Utils'

class Tag {
  constructor (id, name) {
    this.id = id
    this.name = name
  }
}

/**
 * Build a tag with the Builder pattern
 */
class TagBuilder {
  setId (id) {
    if (!isNumber(id)) {
      throw new Error('the id is not a number')
    }
    this.id = id
    return this
  }

  setName (name) {
    if (name === '') {
      throw new Error('the tag name is mandatory')
    }
    this.name = name
    return this
  }

  build () {
    if (this.id === undefined) {
      throw new Error('id is undefined')
    }

    if (this.name === undefined) {
      throw new Error('name is undefined')
    }

    return new Tag(this.id, this.name)
  }
}

export { Tag, TagBuilder }
