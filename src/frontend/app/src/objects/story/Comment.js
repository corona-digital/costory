import { isNumber } from '@/objects/lib/Utils.js'

export class Comment {
  constructor (id, text, author, date) {
    this.id = id
    this.text = text
    this.author = author
    this.date = date
    this.comments = []
  }

  /**
   * Add a response comment to the comment, throw a error if the parameter isn't the instance of Comment
   * @param{Comment} comment
   */
  addComment (comment) {
    if (comment instanceof Comment) {
      this.comments.push(comment)
    } else {
      throw new Error('parameter is not an instance of Comment')
    }
  }
}

/**
 * Build a comment with the Builder pattern
 */
export class CommentBuilder {
  setId (id) {
    if (!isNumber(id)) {
      throw new Error('id is not a number')
    }

    this.id = id
    return this
  }

  setText (text) {
    if (text === '') {
      throw new Error('Text is mandatory')
    }

    this.text = text
    return this
  }

  setAuthor (author) {
    if (author === '') {
      throw new Error('Author is mandatory')
    }
    this.author = author
    return this
  }

  setDate (date) {
    if (!(date instanceof Date)) {
      throw new Error('date not a instance of Date')
    }
    this.date = date
    return this
  }

  setComments (comments) {
    this.comments = comments
    return this
  }

  build () {
    if (this.id === undefined) {
      throw new Error('id is undefined')
    }

    if (this.text === undefined) {
      throw new Error('text is undefined')
    }

    if (this.author === undefined) {
      throw new Error('author is undefined')
    }

    if (this.date === undefined) {
      throw new Error('date is undefined')
    }

    const comment = new Comment(this.id, this.text, this.author, this.date, this.comments)

    if (this.comments !== undefined) {
      this.comments.forEach(element => {
        comment.addComment(element)
      })
    }

    return comment
  }
}
