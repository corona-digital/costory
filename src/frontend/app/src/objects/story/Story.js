import { isNumber } from '@/objects/lib/Utils.js'

import { Paragraph } from '@/objects/story/Paragraph.js'
import { Comment } from '@/objects/story/Comment.js'

export class Story {
  constructor (id, title, excerpt, tags, nbCharacters, nbParagraphsAlreadyWritten, nbMinParagraphs, nbMaxParagraphs, isDone) {
    this.id = id
    this.title = title
    this.excerpt = excerpt
    this.tags = tags
    this.nbCharacters = nbCharacters
    this.nbParagraphsAlreadyWritten = nbParagraphsAlreadyWritten
    this.nbMinParagraphs = nbMinParagraphs
    this.nbMaxParagraphs = nbMaxParagraphs
    this.isDone = isDone
    this.paragraphs = []
    this.comments = []
  }

  /**
   * add a paragraph to the story, throw a error if the parameter isn't a instance of Paragraph
   * @param{Paragraph} paragraph
   */
  addParagraph (paragraph) {
    if (paragraph instanceof Paragraph) {
      this.paragraphs.push(paragraph)
    } else {
      throw new Error('the parameter is not an instance of Paragraph')
    }
  }

  /**
   * add a comment to the story, throw a error if the parameter isn't a instance of Comment
   * @param{Comment} comment
   */
  addComment (comment) {
    if (comment instanceof Comment) {
      this.comments.push(comment)
    } else {
      throw new Error('the parameter is not an instance of Comment')
    }
  }

  /***
   * Shorthand the excerpt of the story
   * @param maxCharacters{Number} Max characters displayed
   * @returns {string} the excerpt shorthanded
   */
  shorthandExcerpt (maxCharacters) {
    let value = this.excerpt
    if (!value) return ''
    value = value.toString()
    if (value.length > maxCharacters) {
      value = value.substring(0, maxCharacters) + ' [...]'
      return value
    } else {
      return value
    }
  }
}

/**
 * Build a story with the Builder pattern
 */
export class StoryBuilder {
  setId (id) {
    if (!isNumber(id)) {
      throw new Error('nbCharacters is not a number')
    }
    this.id = id
    return this
  }

  setTitle (title) {
    if (title === '') {
      throw new Error('title is mandatory')
    }
    this.title = title
    return this
  }

  setExcerpt (excerpt) {
    if (excerpt === '') {
      throw new Error('excerpt is mandatory')
    }
    this.excerpt = excerpt
    return this
  }

  setTags (tags) {
    if (tags.length <= 0) {
      throw new Error('tags should have at least one tag inside')
    }
    this.tags = tags
    return this
  }

  setDone (done) {
    this.done = done
    return this
  }

  setNbCharacters (nbCharacters) {
    if (!isNumber(nbCharacters)) {
      throw new Error('nbCharacters is NaN')
    }
    if (nbCharacters <= 0) {
      throw new Error('nbCharacters should be >= 0')
    }
    this.nbCharacters = nbCharacters
    return this
  }

  setNbParagraphsAlreadyWritten (nbParagraphsAlreadyWritten) {
    if (!isNumber(nbParagraphsAlreadyWritten)) {
      throw new Error('nbParagraphsAlreadyWritten is NaN')
    }
    if (nbParagraphsAlreadyWritten <= 0) {
      throw new Error('nbParagraphsAlreadyWritten should be >= 0')
    }
    this.nbParagraphsAlreadyWritten = nbParagraphsAlreadyWritten
    return this
  }

  setNbMinParagraphs (nbMinParagraphs) {
    if (!isNumber(nbMinParagraphs)) {
      throw new Error('nbMinParagraphs is NaN')
    }
    if (nbMinParagraphs <= 0) {
      throw new Error('nbMinParagraphs should be >= 0')
    }
    this.nbMinParagraphs = nbMinParagraphs
    return this
  }

  setNbMaxParagraphs (nbMaxParagraphs) {
    if (!isNumber(nbMaxParagraphs)) {
      throw new Error('nbMaxParagraphs is NaN')
    }
    if (nbMaxParagraphs <= 0) {
      throw new Error('nbMaxParagraphs should be >= 0')
    }
    this.nbMaxParagraphs = nbMaxParagraphs
    return this
  }

  setParagraphs (paragraphs) {
    if (paragraphs !== null) {
      this.paragraphs = paragraphs
    }
    return this
  }

  setComments (comments) {
    if (comments !== null) {
      this.comments = comments
    }
    return this
  }

  build () {
    if (this.id === undefined) {
      throw new Error('id is undefined')
    }

    if (this.title === undefined) {
      throw new Error('title is undefined')
    }

    if (this.excerpt === undefined) {
      throw new Error('excerpt is undefined')
    }

    if (this.tags === undefined) {
      throw new Error('tags is undefined')
    }

    if (this.nbCharacters === undefined) {
      throw new Error('nbCharacters is undefined')
    }

    if (this.nbParagraphsAlreadyWritten === undefined) {
      throw new Error('nbParagraphsAlreadyWritten is undefined')
    }

    if (this.nbMinParagraphs === undefined) {
      throw new Error('nbMinParagraphs is undefined')
    }

    if (this.nbMaxParagraphs === undefined) {
      throw new Error('nbMaxParagraphs is undefined')
    }

    if (this.done === undefined) { throw new Error('done is undefined') }

    const story = new Story(this.id, this.title, this.excerpt, this.tags, this.nbCharacters, this.nbParagraphsAlreadyWritten, this.nbMinParagraphs, this.nbMaxParagraphs, this.done)

    if (this.paragraphs !== undefined) {
      this.paragraphs.forEach(element => {
        story.addParagraph(element)
      })
    }

    if (this.comments !== undefined) {
      this.comments.forEach(element => {
        story.addComment(element)
      })
    }

    return story
  }
}
