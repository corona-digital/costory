import { isNumber } from '@/objects/lib/Utils.js'

export class Paragraph {
  constructor (id, text, author, date) {
    this.id = id
    this.text = text
    this.author = author
    this.date = date
  }
}

/**
 * Build a paragraph with the Builder pattern
 */
export class ParagraphBuilder {
  setId (id) {
    if (!isNumber(id)) {
      throw new Error('id is not a number')
    }
    this.id = id
    return this
  }

  setText (text) {
    if (text === '') {
      throw new Error('Text is mandatory')
    }

    this.text = text
    return this
  }

  setAuthor (author) {
    if (author === '') {
      throw new Error('Author is mandatory')
    }
    this.author = author
    return this
  }

  setDate (date) {
    this.date = date
    return this
  }

  build () {
    if (this.id === undefined) {
      throw new Error('id is undefined')
    }

    if (this.text === undefined) {
      throw new Error('text is undefined')
    }

    if (this.date === undefined) {
      throw new Error('date is undefined')
    }

    return new Paragraph(this.id, this.text, this.author, this.date)
  }
}
