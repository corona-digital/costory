import { createStore } from 'vuex'

export default createStore({
  state: {
    currentUser: {
      connected: false,
      username: '',
      email: '',
      userId: 0
    }
  },
  mutations: {
    setCurrentUser (state, currentUser) {
      state.currentUser = currentUser
    }
  },
  actions: {},
  modules: {}
})
