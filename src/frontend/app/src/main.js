import { createApp } from 'vue'

import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

import App from './App.vue'
import router from './router'
import store from './store'
import { VueCookieNext } from 'vue-cookie-next'

require('./assets/fontawesome/all.min.css')

window.$ = window.jQuery = require('jquery')

createApp(App)
  .use(store)
  .use(router)
  .use(VueCookieNext)
  .mount('#app')

VueCookieNext.config({
  expire: '7d'
})
