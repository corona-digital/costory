import { createRouter, createWebHistory } from 'vue-router'

// route level code-splitting
// this generates a separate chunk (about.[hash].js) for this route
// which is lazy-loaded when the route is visited.

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/Home')
  },
  {
    path: '/signup',
    name: 'Signup',
    component: () => import('@/views/Signup')
  },
  {
    path: '/browse',
    redirect: '/browse/1/'
  },
  {
    path: '/browse/:num/',
    name: 'Browse',
    props: (route) => ({
      tags: route.query.tags,
      searchTxt: route.query.searchTxt
    }),
    component: () => import('@/views/Browse')
  },
  {
    path: '/story/:num',
    name: 'Story',
    component: () => import('@/views/Story')
  },
  {
    path: '/new-story',
    name: 'New Story',
    component: () => import('@/views/NewStory')
  },
  {
    path: '/story/:num/continue',
    name: 'Story - Continue',
    component: () => import('@/views/StoryContinue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Login')
  },
  {
    path: '/profile',
    name: 'Profile',
    component: () => import('@/views/Profile')
  },
  {
    path: '/:catchAll(.*)',
    name: 'Error 404',
    component: () => import('@/views/NotFound')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
