import { shallowMount } from '@vue/test-utils'
import Pagination from '@/components/Pagination'

describe('Pagination.vue', () => {
  let defaultNbMaxPages

  beforeEach(() => {
    defaultNbMaxPages = Pagination.props.maxPagesDisplayed.default
  })

  it('should render correctly pagination with activeNumber which is equal to 1', () => {
    const wrapper = shallowMount(Pagination, {
      props: {
        size: 30,
        activeNumber: 1
      }
    })

    const pagination = wrapper.find('.pagination')
    const allPages = wrapper.findAll('.page-item')

    const previous = allPages[0]
    const next = allPages[allPages.length - 1]

    expect(pagination.exists()).toBe(true)

    // Check if the previous button is disabled

    expect(previous.classes()).toContain('disabled')

    // Check if the next button is active

    expect(next.classes()).not.toContain('disabled')

    // Check if the numbers are correct

    for (let i = 1; i <= defaultNbMaxPages; ++i) {
      expect(allPages[i].text()).toBe(i.toString())
    }

    // Check if div "..." exists

    expect(allPages[defaultNbMaxPages + 1].exists()).toBe(true)
    expect(allPages[defaultNbMaxPages + 1].text()).toBe('...')

    // Check if the active page is ok

    expect(allPages[1].classes()).toContain('active')
  })

  it('should render correctly pagination with activeNumber between 1 and size', () => {
    const propsPassed = {
      size: 10,
      activeNumber: 3
    }

    const wrapper = shallowMount(Pagination, {
      props: propsPassed
    })

    const allPages = wrapper.findAll('.page-item')

    const previous = allPages[0]
    const next = allPages[allPages.length - 1]

    // Check if the previous button is active

    expect(previous.classes()).not.toContain('disabled')

    // Check if the next button is active

    expect(next.classes()).not.toContain('disabled')

    // Check if div "..." exists

    // 2 because there is the "..." button + the "previous button"

    expect(allPages[defaultNbMaxPages + 2].exists()).toBe(true)
    expect(allPages[defaultNbMaxPages + 2].text()).toBe('...')

    // Check if div "..." exists

    expect(allPages[1].exists()).toBe(true)
    expect(allPages[1].text()).toBe('...')

    // Check if the active page is ok

    expect(allPages[2].classes()).toContain('active')
  })

  it('should render correctly pagination with activeNumber which is near of the size', () => {
    const propsSent = {
      size: 10,
      activeNumber: 7
    }

    const wrapper = shallowMount(Pagination, {
      props: {
        size: propsSent.size,
        activeNumber: propsSent.activeNumber
      }
    })

    const allPages = wrapper.findAll('.page-item')

    const previous = allPages[0]
    const next = allPages[allPages.length - 1]

    // Check if the previous button is active

    expect(previous.classes()).not.toContain('disabled')

    // Check if the next button is active

    expect(next.classes()).not.toContain('disabled')

    // Check if div "..." exists

    expect(allPages[1].exists()).toBe(true)
    expect(allPages[1].text()).toBe('...')

    // Check if the active page is ok

    expect(allPages[1 + defaultNbMaxPages - (propsSent.size - propsSent.activeNumber)].classes()).toContain('active')
  })
})
