import { shallowMount } from '@vue/test-utils'
import { StoryBuilder } from '@/objects/story/Story.js'
import StoryBrowse from '@/components/browse/StoryBrowse.vue'

describe('StoryBrowse.vue', () => {
  it('renders props.story with the good properties', () => {
    const story = new StoryBuilder()
      .setId(1)
      .setTitle('The Lord of the Sheeps')
      .setExcerpt('Cras euismod, massa vitae cursus pulvinar, magna nisl sodales libero, et sodales nibh neque ut nibh. Praesent cursus, justo ut rhoncus malesuada, purus ex bibendum turpis, eu efficitur libero mi vitae enim. Pellentesque tempor, lacus vitae lacinia mollis, nisi ante placerat ex, at malesuada lorem nibh vel quam. Duis at iaculis tellus. Curabitur pellentesque non lorem ut mattis. Duis interdum volutpat lacinia. Etiam ac enim congue arcu tempor eleifend ac non arcu. Curabitur ut ullamcorper mi, eget gravida velit. Sed facilisis tortor ac ornare vestibulum. Nulla elementum dolor quis elit euismod lobortis. Sed vitae risus id ligula congue vulputate. Etiam ut diam eu libero luctus ullamcorper nec vel libero.')
      .setTags([
        {
          name: 'Fantasy'
        },
        {
          name: 'Comedy'
        }
      ])
      .setDone(true)
      .setNbCharacters(1807)
      .setNbParagraphsAlreadyWritten(14)
      .setNbMinParagraphs(5)
      .setNbMaxParagraphs(20)
      .build()

    const wrapper = shallowMount(StoryBrowse, {
      props: {
        story: story
      }
    })
    const title = wrapper.find('.title')
    const excerpt = wrapper.find('.excerpt')
    const tags = wrapper.find('.tags')
    const stats = wrapper.find('.stats')

    expect(title.exists()).toBe(true)
    expect(title.text()).toContain(story.title)

    expect(excerpt.exists()).toBe(true)
    expect(excerpt.text()).toContain(story.shorthandExcerpt(StoryBrowse.props.maxCharacters.default))

    expect(stats.exists()).toBe(true)
    expect(stats.text()).toContain(story.nbMinParagraphs)
    expect(stats.text()).toContain(story.nbMaxParagraphs)
    expect(stats.text()).toContain(story.nbParagraphsAlreadyWritten)

    expect(tags.exists()).toBe(true)

    story.tags.forEach(element => {
      expect(tags.text()).toContain(element.name)
    })
  })

  it('renders props.story.excerpt with a number of characters different if the maxCharacters isn\'t by default', () => {
    const story = new StoryBuilder()
      .setId(1)
      .setTitle('The Lord of the Sheeps')
      .setExcerpt('Cras euismod, massa vitae cursus pulvinar, magna nisl sodales libero, et sodales nibh neque ut nibh. Praesent cursus, justo ut rhoncus malesuada, purus ex bibendum turpis, eu efficitur libero mi vitae enim. Pellentesque tempor, lacus vitae lacinia mollis, nisi ante placerat ex, at malesuada lorem nibh vel quam. Duis at iaculis tellus. Curabitur pellentesque non lorem ut mattis. Duis interdum volutpat lacinia. Etiam ac enim congue arcu tempor eleifend ac non arcu. Curabitur ut ullamcorper mi, eget gravida velit. Sed facilisis tortor ac ornare vestibulum. Nulla elementum dolor quis elit euismod lobortis. Sed vitae risus id ligula congue vulputate. Etiam ut diam eu libero luctus ullamcorper nec vel libero.')
      .setTags([
        {
          name: 'Fantasy'
        },
        {
          name: 'Comedy'
        }
      ])
      .setDone(true)
      .setNbCharacters(1807)
      .setNbParagraphsAlreadyWritten(14)
      .setNbMinParagraphs(5)
      .setNbMaxParagraphs(20)
      .build()

    const maxCharacters = 50

    const wrapper = shallowMount(StoryBrowse, {
      props: {
        story: story,
        maxCharacters: maxCharacters
      }
    })

    const title = wrapper.find('.title')
    const excerpt = wrapper.find('.excerpt')
    const tags = wrapper.find('.tags')
    const stats = wrapper.find('.stats')

    expect(title.exists()).toBe(true)
    expect(title.text()).toContain(story.title)

    expect(excerpt.exists()).toBe(true)
    expect(excerpt.text()).toContain(story.shorthandExcerpt(maxCharacters))

    expect(stats.exists()).toBe(true)
    expect(stats.text()).toContain(story.nbMinParagraphs)
    expect(stats.text()).toContain(story.nbMaxParagraphs)
    expect(stats.text()).toContain(story.nbParagraphsAlreadyWritten)

    expect(tags.exists()).toBe(true)

    story.tags.forEach(element => {
      expect(tags.text()).toContain(element.name)
    })
  })
})
