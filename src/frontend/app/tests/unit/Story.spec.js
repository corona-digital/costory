import { mount, shallowMount } from '@vue/test-utils'
import ParagraphView from '@/components/story/Paragraph.vue'
import CommentView from '@/components/story/Comment.vue'
import { StoryBuilder } from '@/objects/story/Story.js'
import { ParagraphBuilder } from '@/objects/story/Paragraph.js'
import { CommentBuilder } from '@/objects/story/Comment.js'
import { formatDate } from '@/objects/lib/Utils.js'
import { TagBuilder } from '@/objects/tag/Tag'
import StoryDetails from '@/components/story/StoryDetails'

describe('StoryDetails.vue', () => {
  it('render pros.story with all paragraphs', () => {
    const tags = [
      new TagBuilder()
        .setId(1)
        .setName('Roman')
        .build(),
      new TagBuilder()
        .setId(2)
        .setName('Fantasy')
        .build()
    ]

    const story = new StoryBuilder()
      .setId(1)
      .setTitle('The Ouicher')
      .setTags(tags)
      .setExcerpt('default')
      .setNbCharacters(1808)
      .setNbMinParagraphs(10)
      .setNbMaxParagraphs(25)
      .setDone(true)
      .setNbParagraphsAlreadyWritten(3)
      .build()

    const wrapper = shallowMount(StoryDetails, {
      props: {
        story: story
      }
    })

    const title = wrapper.find('.title')
    const storyInformation = wrapper.find('.story_information')

    expect(title.exists()).toBe(true)
    expect(title.text()).toContain(story.title)

    expect(storyInformation.exists()).toBe(true)
    expect(storyInformation.text()).toContain(story.nbMinParagraphs)
    expect(storyInformation.text()).toContain(story.nbMaxParagraphs)
    expect(storyInformation.text()).toContain(story.nbParagraphsAlreadyWritten)
  })

  it('render paragraphs should be correct', () => {
    const p1 = new ParagraphBuilder()
      .setId(1)
      .setText('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent consequat ante vel quam viverra, vitae mollis libero accumsan. Donec mollis tempus suscipit. Vivamus quam eros, dictum in urna eget, lobortis fringilla dui. Suspendisse fringilla turpis sit amet turpis posuere faucibus. Cras consequat vulputate neque, non gravida nisl viverra vel. In ac pellentesque nunc. Maecenas vitae ante id libero aliquet semper quis ut ligula. Cras ut venenatis massa. Pellentesque rhoncus vulputate diam, sit amet mollis urna viverra pretium. Donec ac erat odio. Fusce bibendum urna id dapibus pharetra. Pellentesque tempor dui ut lacinia commodo. Aliquam erat volutpat. Cras ut vulputate mauris.')
      .setAuthor('Fukushimiste')
      .setDate(new Date(2020, 8, 19))
      .build()

    const wrapper = shallowMount(ParagraphView, {
      props: {
        paragraph: p1
      }
    })

    const paragraph = wrapper.find('.text')
    const detail = wrapper.find('.detail')

    expect(paragraph.exists()).toBe(true)
    expect(paragraph.text()).toContain(p1.text)

    expect(detail.exists()).toBe(true)
    expect(detail.text()).toContain(p1.author)
    expect(detail.text()).toContain(formatDate(p1.date))
  })

  it('Comment should render correctly', () => {
    const c1 = new CommentBuilder()
      .setId(1)
      .setText('Not a very good story')
      .setAuthor('Juri')
      .setDate(new Date(2020, 8, 22))
      .build()

    const wrapper = shallowMount(CommentView, {
      props: {
        comment: c1
      }
    })

    const commentText = wrapper.find('.text')
    const detail = wrapper.find('.detail')

    expect(commentText.exists()).toBe(true)
    expect(commentText.text()).toContain(c1.text)

    expect(detail.exists()).toBe(true)
    expect(detail.text()).toContain(c1.author)
    expect(detail.text()).toContain(formatDate(c1.date))
  })

  it('Comment should render correctly if contains response', () => {
    const c2 = new CommentBuilder()
      .setId(2)
      .setText('No u')
      .setAuthor('Fukushimiste')
      .setDate(new Date(2020, 8, 22))
      .build()

    const c1 = new CommentBuilder()
      .setId(1)
      .setText('Not a very good story')
      .setAuthor('Juri')
      .setDate(new Date(2020, 8, 22))
      .build()

    c1.addComment(c2)

    const wrapper = mount(CommentView, {
      props: {
        comment: c1
      }
    })

    const commentText = wrapper.find('.text')
    const detail = wrapper.find('.detail')
    const responseText = wrapper.find('.response  .text')
    const responseDetail = wrapper.find('.response  .detail')

    expect(commentText.exists()).toBe(true)
    expect(commentText.text()).toContain(c1.text)

    expect(detail.exists()).toBe(true)
    expect(detail.text()).toContain(c1.author)
    expect(detail.text()).toContain(formatDate(c1.date))

    expect(responseText.exists()).toBe(true)
    expect(responseText.text()).toContain(c2.text)

    expect(responseDetail.exists()).toBe(true)
    expect(responseDetail.text()).toContain(c2.author)
    expect(responseDetail.text()).toContain(formatDate(c2.date))
  })
})
