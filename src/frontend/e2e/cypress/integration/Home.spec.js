describe('Click on button in the home page should redirect to the correct page', () => {
    before(() =>{
        cy.resetDb()
    })

    beforeEach(() => {
        cy.visit('/')
    })

    it('Should redirect to the signup page when the user click on \"start cur writing adventure\"', () => {
        cy.get('#btn_new_story').click()
        cy.location().should(loc => expect(loc.pathname).to.eq('/new-story'))
    })

    it('Should redirect to the browse page when the user click on \"Browse stories\"', () => {
        cy.get('#btn_browse').click()
        cy.location().should(loc => expect(loc.pathname).to.contain('/browse/1'))
    })
})
