const Chance = require('chance')

describe('An anonymous can\'t add comment', () => {

  const title = chance.sentence({ words: 3 })
  const paragraph = chance.string({ length: 200 })

  before(() => {
    cy.resetDb()

    cy.writeAStory(title, paragraph, [
      'Fantasy'
    ])

  })

  it('should see a button to invite anonymous user to create a account', () => {
    cy.get('.notConnected').contains('Create a account')
  })

  it('should not let a anonymous user to answer a comment', () => {
    cy.get('.reviews .bnt-response').should('not.exist')
  })
})

describe('A connected user can comment on a story', function () {

  const title = chance.sentence({ words: 3 })
  const paragraph = chance.string({ length: 200 })

  const username = chance.string()
  const email = chance.email()
  const password = chance.string()

  before(() => {
    cy.createAUser(username, email, password)

    cy.writeAStory(title, paragraph, [
      'Fantasy'
    ])
  })

  beforeEach(() => {
    cy.login(username, password)

    cy.get('.story').contains(title).parents('.story').within(() => {
      cy.get('.explore a').click()
    })

  })

  it('should let the user add a comment', () => {
    const cText = chance.string()
    cy.get('#txt_comment').type(cText)
    cy.get('[type=submit]').click()
    cy.get('.comments-section').contains(cText)
  })

  it('should let the user answer a comment', () => {
    const cText = chance.string()
    cy.get('.reviews .btn-response').first().click()
    cy.get('.reviews #txt_comment').first().type(cText)
    cy.get('.reviews .addComment').first().click()
    cy.get('.reviews .response').first().contains(cText)

  })

  it('should not let the user add answer to an answer', function () {
    cy.get('.reviews .response .bnt-response').should('not.exist')

  })
})
