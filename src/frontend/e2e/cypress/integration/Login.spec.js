const Chance = require('chance')

var chance = new Chance()

const username = chance.string()
const email = chance.email()
const password = chance.string()

describe('An user should getting an feedback when he tries to log in', () => {

    before(() => {
        cy.resetDb()

        cy.createAUser(username, email, password)
    })

    beforeEach(() => {
        cy.visit('login')

        cy.get('#username').type(username)
        cy.get('#password').type(password)

        cy.clearCookie('sid')
        cy.clearCookie('username')
        cy.clearCookie('connected')
        cy.clearCookie('email')

        cy.get('[type=submit]').focus()
    })

    it('Should display an valid feedback when everything is fine', () => {
        cy.get('#username').should('have.class','is-valid')
        cy.get('#password').should('have.class','is-valid')
    })

    it('Should display an invalid feedback when the username is empty', () => {
        cy.get('#username').clear()
        cy.get('[type=submit]').focus()

        cy.contains('Username required')
        cy.get('.invalid-feedback')
        cy.get('#username').should('have.class', 'is-invalid')
    })

    it('Should display an invalid feedback when the password is empty', () => {
        cy.get('#password').clear()
        cy.get('[type=submit]').focus()

        cy.contains('Password required')
        cy.get('.invalid-feedback')
        cy.get('#password').should('have.class', 'is-invalid')
    })

    it('Should display an success modal when the Login form works', () => {
        cy.get('[type=submit]').click()

        cy.get('#success').should('be.visible')
        cy.get('#success .title').contains('Great')
        cy.get('#success .description').contains('You have been successfully logged in')
    })

    it('Should redirect to /browse when the Login form works', () => {
        cy.get('[type=submit]').click()
        cy.get('#success a').click()
        cy.url().should('include', '/browse/1')
    })

    it('Should display an error modal when the Login form doesn\'t work ', () => {
        cy.get('#password').type(chance.string())
        cy.get('[type=submit]').click()
        cy.get('#error').should('be.visible')
        cy.get('#error .title').contains('Oh nio!')
    })

    it('Should display a different navigation bar when the user is logged in', () => {
        cy.get('[type=submit]').click()

        cy.get('#success a').click()
        cy.get('.btn-menu').contains(username)
    })

    it('Should logout the user', () =>  {
        cy.get('[type=submit]').click()


        cy.get('#success a').click()
        cy.get('.btn-menu').click()
        cy.get('#logout').click()

        cy.get('.btn-menu').should('not.be.visible')
    })
})
