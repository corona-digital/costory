const Chance = require('chance')

var chance = new Chance()

const username = chance.string()
const email = chance.email()
const password = chance.string()

describe('An user should getting a feedback when he signs up', () => {
  before(()=>{
    cy.resetDb()
  })

  beforeEach(() => {
    cy.visit('signup')

    cy.get('#username').type(username)
    cy.get('#email').type(email)
    cy.get('#password').type(password)
    cy.get('#cpassword').type(password)

    cy.get('[type=submit]').focus()
  })

  it('Should display a valid feedback when everything is fine', () => {
    cy.get('#username').should('have.class', 'is-valid')
    cy.get('#email').should('have.class', 'is-valid')
    cy.get('#password').should('have.class', 'is-valid')
    cy.get('#cpassword').should('have.class', 'is-valid')
  })

  it('Should display an invalid feedback when the username is empty', () => {
    cy.get('#username').clear()
    cy.get('[type=submit]').focus()

    cy.contains('Username required')
    cy.get('.invalid-feedback')
    cy.get('#username').should('have.class', 'is-invalid')
  })

  it('Should display an invalid feedback when the email is empty', () => {
    cy.get('#email').clear()
    cy.get('[type=submit]').focus()

    cy.contains('E-mail required')
    cy.get('.invalid-feedback')
    cy.get('#email').should('have.class', 'is-invalid')
  })

  it('Should display an invalid feedback when the password and repeat password is false', () => {
    cy.get('#cpassword').type('wesh')
    cy.get('[type=submit]').focus()

    cy.contains('Confirmation of password and password don\'t match')

    cy.get('#password').should('have.class', 'is-invalid')
    cy.get('#cpassword').should('have.class', 'is-invalid')
  })

  it('Should display an success modal when the Sign Up form works', () => {
    cy.get('[type=submit]').click()
    cy.get('#success').should('be.visible')
    cy.get('#success .title').contains('Great')
    cy.get('#success .description').contains('Your account has been created successfully')
  })

  it('Should display an error modal when the Sign Up form doesn\'t work ', () => {
    cy.get('[type=submit]').click()
    cy.get('#error').should('be.visible')
    cy.get('#error .title').contains('Oh nio')
  })
})
