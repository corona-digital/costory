const Chance = require('chance')

const chance = new Chance()

const username = chance.string()
const email = chance.email()
const password = chance.string()

const title = chance.sentence({ words: 3 })
const paragraph = chance.string({ length: 200 })

describe('An connected user can create a new story ', () => {
  before(() => {
    cy.resetDb()

    // Create a account
    cy.createAUser(username, email, password)
    cy.login(username, password)
  })

  beforeEach(() => {
    // Setup basic input for the story
    cy.visit('new-story')

    cy.get('#tag-input').type('a')
    cy.get('#tags-result').children().first().click()

    cy.get('#title').type(title)
    cy.get('#paragraph').type(paragraph)

  })

  it('Should have at least one tag in the suggestion list', () => {
    cy.get('#remove-badge i').click()
    cy.get('#tag-input').type('a')
    cy.get('#tags-result').children().should('to.not.be.empty')
  })

  it('Should display a valid feedback when everything is fine', () => {
    cy.get('#title').should('have.class', 'is-valid')
  })

  it('Should display an invalid feedback when the title is empty', () => {
    cy.get('#title').clear()
    cy.get('[type=submit]').focus()

    cy.contains('Title required')
    cy.get('.invalid-feedback')
    cy.get('#title').should('have.class', 'is-invalid')
  })

  it('Should display an invalid feedback when the paragraph is empty ', () => {

    cy.get('#paragraph textarea').clear()

    cy.get('[type=submit]').focus()

    cy.contains('Paragraph cannot be empty...')
  })

  it('Should display an invalid feedback when the number of character in paragraph exceed the maximum ', () => {
    const text = chance.string({ length: 260 })

    cy.get('#paragraph textarea').clear().type(text)
    cy.get('[type=submit]').focus()

    cy.get('#paragraph').contains('Number of maximum characters exceeded')
  })

  it('Should display an invalid feedback when min paragraph is zero', () => {
    cy.get('#paragraph-min').clear().type('0')
    cy.get('[type=submit]').focus()

    cy.contains('Something is wrong with these values')
    cy.get('.error')
  })

  it('Should display an invalid feedback when min paragraph is superior to max paragraph', () => {
    cy.get('#paragraph-min').clear().type('30')
    cy.get('[type=submit]').focus()

    cy.contains('Something is wrong with these values')
    cy.get('.error')
  })

  it('Should display an invalid feedback when max paragraph is inferior to min paragraph', () => {
    cy.get('#paragraph-max').clear().type('4')
    cy.get('[type=submit]').focus()

    cy.contains('Something is wrong with these values')
    cy.get('.error')
  })

  it('Should display an invalid feedback zero tag is selected', () => {
    cy.get('#remove-badge i').click()

    cy.get('[type=submit]').focus()

    cy.contains('Require at least one tag')
    cy.get('.invalid-feedback')

  })

  it('Should display an success modal when the new story form works', () => {

    cy.get('[type=submit]').click()

    cy.get('#success').should('be.visible')
    cy.get('#success').contains('Great !')
  })

  it('Should redirect to /story/{id} when the new story form works', () => {

    cy.get('[type=submit]').click()
    cy.get('#success a').click()

    cy.url().should('include', '/story/')
  })

})

describe('An anonymous user can create a new story ', () => {
  beforeEach(() => {
    cy.visit('new-story')


    cy.get('#title').type(title)
    cy.get('#paragraph').type(paragraph)

    cy.get('#tag-input').type('a')
    cy.get('#tags-result').children().first().click()

    cy.get('[type=submit]').focus()
  })

  it('Should display an success modal when the new story form works', () => {
    cy.get('[type=submit]').click()
    cy.get('#success').should('be.visible')
    cy.get('#success').contains('Great !')
  })

})

