const Chance = require('chance')

var chance = new Chance()

const username = chance.string()
const email = chance.email()
const password = chance.string()

const story1Title = chance.sentence({ words: 3 })
const story1Paragraph = chance.string({ length: 200 })

const story2Title = chance.sentence({ words: 3 })
const story2Paragraph = chance.string({ length: 200 })

const story3Title = chance.sentence({ words: 3 })
const story3Paragraph = chance.string({ length: 200 })

const newParagraph = chance.string({ length: 200 })

describe('Profile.vue', () => {
  before(() => {
    cy.resetDb()

    cy.writeAStory(story1Title, story1Paragraph, ['Fantasy'])

    cy.createAUser(username, email, password)
    cy.login(username, password)

    cy.writeAStory(story2Title, story2Paragraph, ['Fantasy'])
    cy.writeAStory(story3Title, story3Paragraph, ['Fantasy'])

    cy.addAParagraph(story1Title, newParagraph)

    cy.visit('profile')

  })

  it('should display the profile when the user click on the nav bar', function () {
    cy.get('.btn-menu').click()
    cy.get('#profile').click()

    cy.location().should(loc => expect(loc.pathname).to.eq('/profile'))
  })

  it('should display the username', function () {
    cy.get('.username-container').contains(username)
  })

  it('should display the email', function () {
    cy.get('.email-container').contains(email)
  })

  it('should display that the user create 2 stories', function () {
    cy.get('#created').contains(2)
  })

  it('should display that the user participated to 1 story', function () {
    cy.get('#participated').contains(1)
  })

  it('should redirect to the story when the user click on it', function () {
    cy.get('#created-list').within(() => {
      cy.get('.card').first().click()
    })

    cy.location().should(loc => expect(loc.pathname).to.contain('/story'))
  })
})
