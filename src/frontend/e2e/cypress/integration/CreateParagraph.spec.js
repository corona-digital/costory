const Chance = require('chance')

const chance = new Chance()

const username = chance.string()
const email = chance.email()
const password = chance.string()

const title = chance.sentence({ words: 3 })
const paragraph = chance.string({ length: 200 })

const anotherParagraph = chance.string({length: 200})
const paragraphWithExceedLength = chance.string({length: 500})

const anotherParagraph1 = chance.string({length: 200})
const anotherParagraph2 = chance.string({length: 200})

describe('An anonymous user can create a new paragraph ', () => {
    before(() => {
        cy.resetDb()

        cy.writeAStory(title, paragraph, [
            "Roman"
        ])

        cy.get('.continue-the-story .btn').click()
    })

    beforeEach(() => {
        cy.get('#paragraph textarea').clear()
    })

    it('Should return an valid feedback when everything is fine', () => {
        cy.get('#paragraph').type(anotherParagraph)
        cy.get('#paragraph textarea').should('have.class','is-valid')
    })

    it('Should return an invalid feedback when the paragraph is empty', () => {
        cy.get('#paragraph').type(chance.string())
        cy.get('#paragraph textarea').clear()
        cy.get('#paragraph textarea').should('have.class','is-invalid')
        cy.get('#paragraph').contains('Paragraph cannot be empty')
    })

    it('Should return an invalid feedback when the characters exceed the max characters', () => {
        cy.get('#paragraph').type(paragraphWithExceedLength)
        cy.get('#paragraph textarea').should('have.class','is-invalid')
        cy.get('#paragraph').contains('Number of maximum characters exceeded')
    })

    it('Shouldn\'t let the anonymous create a paragraph when not everything is fine', () => {
        cy.get('.btn-continue-story').click()
        cy.get('#paragraph textarea').should('have.class','is-invalid')
    })

    it('Should let the anonymous create the paragraph', () => {
        cy.get('#paragraph').type(anotherParagraph)
        cy.get('.btn-continue-story').click()
        cy.get('.paragraph:last-child').contains(paragraph)
    })
})

describe('An connected user can create a new paragraph', () => {
    before(() => {

        // Sign up a connected user

        cy.createAUser(username, email, password)
        cy.login(username, password)

        // Create a new story
        cy.writeAStory(title, paragraph, [
            "Roman"
        ])

        cy.get('.continue-the-story .btn').click()
    })

    beforeEach(() => {
        cy.url().should('include', '/continue')
        cy.get('#paragraph textarea').clear()
    })

    it('Should let an connected user to create a paragraph', () => {
        cy.get('#paragraph').type(anotherParagraph)
        cy.get('.btn-continue-story').click()
        cy.get('.paragraph:last-child').contains(paragraph)
    })
})

describe('An anonymous user can end a story', function () {

    const min = 5
    const max = 10

    const smallParagraph = chance.string({
        length: 10
    })

    beforeEach(() => {
        cy.writeAStoryWithMinMax(title, smallParagraph, [
            "Fantasy"
        ], min, max)

        cy.get('.continue-the-story .btn').click()
    })

    it('Should let an anonymous user to end the story', () => {

        for(let i = 0; i < min - 1; i++) {
            cy.get('#paragraph').type(smallParagraph)
            cy.get('.btn-continue-story').click()
            cy.get('.continue-the-story .btn').click()
        }

        cy.get('#paragraph').type(smallParagraph)
        cy.get('.btn-end-story').click()

        cy.url().should('not.include', '/continue')

        cy.get('.paragraph').each(($value, index,$list) => {
            cy.wrap($value).contains(smallParagraph)
        })
    })

    it('Shouldn\'t let an anonymous user to end the story if the condition of nb. min of paragraphs isn\'t filled',  () => {

        cy.get('#paragraph').type(smallParagraph)
        cy.get('.btn-end-story').click()

        cy.get('.btn-end-story').should('have.class', 'disabled')
        cy.url().should('include', '/continue')
    })
});


describe('An connected user can end a story', function () {

    const min = 5
    const max = 10

    const smallParagraph = chance.string({
        length: 10
    })

    before(() => {

        // Sign up a connected user

        cy.visit('signup')

        cy.get('#username').type(username)
        cy.get('#email').type(email)
        cy.get('#password').type(password)
        cy.get('#cpassword').type(password)

        cy.get('[type=submit]').click()

        // Login as the new user created

        cy.visit('login')

        cy.get('#username').type(username)
        cy.get('#password').type(password)

        cy.clearCookie('sid')
        cy.clearCookie('username')
        cy.clearCookie('connected')
        cy.clearCookie('email')

        cy.get('[type=submit]').click()

        cy.get('#success a').click()
    })

    beforeEach(() => {
        cy.writeAStoryWithMinMax(title, smallParagraph, [
            "Fantasy"
        ], min, max)

        cy.get('.continue-the-story .btn').click()
    })

    it('Should let an connected user to end the story', () => {

        for(let i = 0; i < min - 1; i++) {
            cy.get('#paragraph').type(smallParagraph)
            cy.get('.btn-continue-story').click()
            cy.get('.continue-the-story .btn').click()
        }

        cy.get('#paragraph').type(smallParagraph)
        cy.get('.btn-end-story').click()

        cy.url().should('not.include', '/continue')

        cy.get('.paragraph').each(($value, index,$list) => {
            cy.wrap($value).contains(smallParagraph)
        })
    })

    it('Shouldn\'t let an connected user to end the story if the condition of nb. min of paragraphs isn\'t filled',  () => {

        cy.get('#paragraph').type(smallParagraph)
        cy.get('.btn-end-story').click()

        cy.get('.btn-end-story').should('have.class', 'disabled')
        cy.url().should('include', '/continue')
    })
});
