const Chance = require('chance')

const chance = new Chance()

const title = chance.string()
const paragraph = chance.string({
  length: 200
})

const repeatWrite = 2

describe('An anonymous user can browse the stories', () => {

  before(() => {
    cy.resetDb()
    for (let i = 0; i < repeatWrite; i++) {
      cy.writeAStory(title, paragraph, [
        'Fantasy'
      ])
    }
  })

  beforeEach(() => {
    cy.visit('browse')
  })

  it('Should return all the stories filtered by tag', () => {
    cy.selectTagsFilter([
      'Fantasy'
    ])
    cy.get('form [type=submit]').click()
    cy.get('.stories').find('.story').should('have.length', repeatWrite)
  })

  it('Should return only the story with the exact tags', () => {
    const tagsChosen = [
      'Adventure',
      'Romance'
    ]

    cy.writeAStory(title, paragraph, tagsChosen)

    cy.visit('browse')

    cy.selectTagsFilter(tagsChosen)
    cy.get('form [type=submit]').click()
    cy.get('.stories').find('.story').should('have.length', 1)
  })

  it('Should return only the stores with the title like', () => {
    const title = chance.string()
    const paragraph = chance.string({
      length: 200
    })

    cy.writeAStory(title, paragraph, [
      'Horror'
    ])

    cy.visit('browse')

    cy.get('form [type=search]').type(title.substr(title.length / 2, 10))
    cy.get('form [type=submit]').click()
    cy.get('.stories').find('.story').should('have.length', 1).contains(title)
  })

  it('Should found the searched story when we combine tag search and input search', function () {
    const title = 'Costory'
    const tag = ['Fantasy']

    cy.writeAStory(title, chance.string({ lenght: 10 }), tag)

    cy.visit('browse')

    cy.get('form [type=search]').type(title.substr(title.length / 2, 10))
    cy.selectTagsFilter(tag)
    cy.get('form [type=submit]').click()
    cy.get('.stories').find('.story').should('have.length', 1).contains(title)
  })
})
