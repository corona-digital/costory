// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('writeAStoryWithMinMax', (title, paragraph, tags, min, max) => {
  cy.visit('new-story')

  cy.get('#paragraph-min').clear()
  cy.get('#paragraph-max').clear()
  cy.get('#paragraph-min').type(min)
  cy.get('#paragraph-max').type(max)

  cy.get('#title').type(title)
  cy.get('#paragraph').type(paragraph)

  for (const tag of tags) {
    cy.get('#tag-input').type(tag)
    cy.get('#tags-result .result-item').focus()
    cy.get('#tags-result .result-item').click()
  }

  cy.get('[type=submit]').click()
  cy.get('#success a').click()
})

Cypress.Commands.add('writeAStory', (title, paragraph, tags) => {
  cy.visit('new-story')

  cy.get('#title').type(title)
  cy.get('#paragraph').type(paragraph)

  for (const tag of tags) {
    cy.get('#tag-input').type(tag)
    cy.get('#tags-result .result-item').focus()
    cy.get('#tags-result .result-item').click()
  }

  cy.get('[type=submit]').click()
  cy.get('#success a').click()
})

Cypress.Commands.add('selectTagsFilter', (tags) => {
  cy.get('#select-tags-filter .btn-select-options').click()

  for (const tag of tags) {
    cy.get('#select-tags-filter').contains(tag).click()
  }

})

Cypress.Commands.add('createAUser', (username, email, password) => {
  cy.visit('signup')

  cy.get('#username').type(username)
  cy.get('#email').type(email)
  cy.get('#password').type(password)
  cy.get('#cpassword').type(password)

  cy.get('[type=submit]').click()
  cy.get('#success a').click()

})

Cypress.Commands.add('login', (username, password) => {
  cy.visit('login')

  cy.get('#username').type(username)
  cy.get('#password').type(password)

  cy.clearCookie('id')
  cy.clearCookie('username')
  cy.clearCookie('connected')
  cy.clearCookie('email')

  cy.get('[type=submit]').click()
  cy.get('#success a').click()

})

Cypress.Commands.add('addAParagraph', (title, paragraph) => {
  cy.visit('browse')

  cy.get('.story').contains(title).parents('.story').within(() => {
    cy.get('.explore a').click()
  })
  cy.get('.continue-the-story a').click()

  cy.get('#paragraph').type(paragraph)
  cy.get('.btn-continue-story').click()
})

Cypress.Commands.add('resetDb', () => {
  cy.request({
    method: 'PUT',
    url: Cypress.config('apiUrl') + '/test/resetDb',
    headers: {
      'origin': 'http://localhost'
    }
  }).then((resp) => {
    expect(resp.status).to.eq(200)
  })
})

