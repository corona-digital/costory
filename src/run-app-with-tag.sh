#!/bin/bash

docker-compose down
tag=$1 docker-compose -f docker-compose.yml -f docker-compose.local.yml up -d db
tag=$1 docker-compose -f docker-compose.yml -f docker-compose.local.yml up -d backend
tag=$1 docker-compose -f docker-compose.yml -f docker-compose.local.yml up -d frontend
docker-compose up -d reverse
