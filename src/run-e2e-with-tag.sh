#!/bin/bash

echo "tag : $1 "

if test -z "$1"
then
  echo "missing arg"
  exit 1
else
sh ./run-app-with-tag.sh $1
tag=$1 docker-compose -f docker-compose.yml -f docker-compose.test.yml -f docker-compose.local.yml up cypress

CONTAINER_ID=$(docker ps -aqf "name=costory_e2e")
EXIT_CODE=$(docker inspect $CONTAINER_ID --format='{{.State.ExitCode}}')

docker-compose down --remove-orphans

exit $EXIT_CODE

fi
