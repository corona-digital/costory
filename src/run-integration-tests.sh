#!/bin/bash

tag=local

tag=$tag docker-compose -f docker-compose.yml -f docker-compose.test.yml build backend
sh ./run-integration-tests-with-tags.sh $tag
