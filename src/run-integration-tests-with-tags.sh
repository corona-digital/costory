#!/bin/bash

docker-compose down

tag=$1 docker-compose -f docker-compose.yml -f docker-compose.local.yml build db
tag=$1 docker-compose -f docker-compose.yml -f docker-compose.local.yml up -d db
tag=$1 docker-compose -f docker-compose.yml  -f docker-compose.local.yml -f docker-compose.test.yml up backend


CONTAINER_ID=$(docker ps -aqf "name=costory_backend")
EXIT_CODE=$(docker inspect $CONTAINER_ID --format='{{.State.ExitCode}}')

docker-compose down

exit $EXIT_CODE
