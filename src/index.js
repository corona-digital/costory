//Entry point
const express = require('express');
const bodyParser = require('body-parser')
const app = express();

//For parsing the body of POST request
app.use(bodyParser.urlencoded({ extended: false }))

const port = process.env.PORT || 8080;
app.listen(port, () => {
    console.log(`listening on ${port}`);
});
