const express = require('express');
const database = require('../service/db');

const router = express.Router();
const comment = database.comments;

/**
 * Add a comment to either a story or another comment
 * @param req
 * @returns {Promise<unknown>}
 */
router.addcomment = function (req) {
  return new Promise((res, rej) => {
    if (req.text === '') {
      rej(Error('Content is empty'));
    } else {
      comment.create({
        tblStoryId: req.tblStoryId,
        text: req.text,
        note: req.note,
        tblUserId: req.tblUserId,
        tblCommentId: req.tblCommentId,
        date: new Date(),
      }).then((resp) => {
        res(resp);
      }).catch((error) => {
        rej(Error(error));
      });
    }
  });
};

/**
 * Get a comment by id
 * If no comment founded
 * @Return 404 status code
 * Else
 * @Return 200 status code
 */
router.get('/:id', (req, res) => {
  comment.findOne({
    where: {
      id: req.params.id,
    },
    include: ['user'],
  }).then((r) => {
    if (r == null) {
      res.status(404);
      res.send(`No comment with id ${req.params.id}`);
      return;
    }
    res.send(r);
  });
});

/**
 * Get all comments appended with author (user model) informations.
 */
router.get('/', (req, res) => {
  comment.findAll({
    include: ['user'],
  }).then((r) => res.send(r));
});

// Add a comment
router.post('/', (req, res) => {
  const payload = {
    tblStoryId: req.body.tblStoryId,
    text: req.body.text,
    tblUserId: req.body.tblUserId,
    tblCommentId: req.body.tblCommentId,
    sessionID: req.sessionID,
  };
  router.addcomment(payload).then((result) => {
    res.status(201).send(result);
  }).catch((reason) => {
    console.log(reason);
    res.status(403).send(reason);
  });
});

module.exports = router;
