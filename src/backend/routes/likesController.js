const express = require('express');

const router = express.Router();

const database = require('../service/db');

const { likes } = database;

/**
 * @Return all likes
 */
router.get('/', (req, res) => {
  likes.findAll().then((r) => res.send(r));
});

/**
 * Count and return all vote on a story
 * @param storyId
 * @returns {Promise<unknown>}
 */
router.countVote = function (storyId) {
  return new Promise((res, rej) => {
    likes.findAll({
      where: {
        tblStoryId: storyId,
      },
    }).then((r) => {
      let sum = 0;
      r.forEach((entry) => {
        sum += entry.dataValues.value;
      });
      res(sum.toString());
    }).catch((err) => rej(err));
  });
};

/**
 * Add a like (or dislike) to a story
 */
router.post('/', (req, res) => {
  const payload = {
    tblStoryId: req.body.tblStoryId,
    tblUserId: req.body.tblUserId,
    value: req.body.value,
  };
  if (!['-1', '0', '1'].includes(payload.value)) {
    res.send(400);
    res.send('Invalid Like Value, must be [-1,0,1]');
  } else {
    likes.findOrCreate({
      where: {
        tblStoryId: payload.tblStoryId,
        tblUserId: payload.tblUserId,
      },
      // values to put if nothing was found
      defaults: {
        tblStoryId: payload.tblStoryId,
        tblUserId: payload.tblUserId,
        value: payload.value,
      },
    }).then((r) => {
      // result of findOrCreate returns a boolean that tells if it was found or created
      const created = r[1];
      if (!created) {
        if (payload.value !== '0') {
          likes.update({
            value: payload.value,
          },
          {
            where: {
              tblStoryId: payload.tblStoryId,
              tblUserId: payload.tblUserId,
            },
          });
        } else {
          // delete link if value = 0
          likes.destroy({
            where: {
              tblStoryId: payload.tblStoryId,
              tblUserId: payload.tblUserId,
            },
          });
        }
      }
    }).then((r) => {
      res.send(r);
    }).catch((err) => {
      res.status(400);
      res.send(err);
    });
  }
});

module.exports = router;
