const express = require('express');

const router = express.Router();
const database = require('../service/db');

const tag = database.tags;

/**
 * Get all tags
 */
router.get('/', (req, res) => {
  tag.findAll().then((r) => res.send(r));
});

/**
 * Get a tags by id
 */
router.get('/:id', (req, res) => {
  tag.findOne({
    where: {
      id: req.params.id,
    },
  }).then((r) => {
    if (r == null) {
      res.status(404);
      res.send(`No tag with id ${req.params.id}`);
      return;
    }
    res.send(r);
  });
});

module.exports = router;
