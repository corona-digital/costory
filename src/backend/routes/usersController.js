const express = require('express');

const router = express.Router();
const bcrypt = require('bcrypt');
const database = require('../service/db');

/**
 * Get a user by id
 */
const user = database.users;
router.get('/:id', (req, res) => {
  user.findOne({
    where: {
      id: req.params.id,
    },
  }).then((r) => {
    if (r == null) {
      res.status(404);
      res.send(`No user with id ${req.params.id}`);
      return;
    }
    res.send(r);
  });
});

/**
 * Get all users
 */
router.get('/', (req, res) => {
  user.findAll().then((r) => res.send(r));
});

/**
 * Add a user
 */
router.post('/', (req, res) => {
  // TODO check input validation
  bcrypt.hash(req.body.password, 10).then((hash, err) => {
    if (err) {
      res.send(err);
    } else {
      user.create({
        username: req.body.username,
        email: req.body.email,
        password: hash,
        isAdmin: false,
      }).then((resp) => res.status(201).send(resp))
        .catch((error) => {
          console.log(error);
          res.status(400).send(error);
        });
    }
  });
});

module.exports = router;
