const express = require('express');
const bcrypt = require('bcrypt'); // Password encryption
const database = require('../service/db');
const locker = require('./concurrentController');

const user = database.users;
const router = express.Router();

/**
 * Login endpoint
 * If login is successful or already logged:
 * @Return 200 status code
 * Else
 * @Return 401 status code
 */
router.post('/login', (req, res) => {
  if (req.session.loggedin) {
    res.status(200);
    res.send('Already logged');
    return;
  }

  // Check payload
  if (req.body.username === undefined
      || req.body.password === undefined
      || req.body.username === ''
      || req.body.password === '') {
    res.status(401);
    res.send('Login error!');
  }

  // Try to fetch the user
  user.findOne({
    where: {
      username: req.body.username,
    },
  }).then((userReturned) => {
    // Check is user exist
    if (userReturned === null) {
      res.status(401);
      res.send('Login error!');
      return;
    }
    // Check password validity
    bcrypt.compare(req.body.password, userReturned.password).then((isSame) => {
      if (isSame) {
        req.session.loggedin = true;
        req.session.username = userReturned.username;
        req.session.userId = userReturned.id;
        const payload = {
          username: userReturned.username,
          email: userReturned.email,
          id: userReturned.id,
        };
        // Return the user infos
        res.status(200);
        res.send(payload);
      } else {
        res.status(401);
        res.send('Login error!');
      }
    });
  }).catch((error) => {
    res.status(401);
    res.send(error);
  });
});
/**
 * Logout endpoint
 * Unlock access to every stories the user had locked with his session-id
 * @Return 200 status code
 */
router.get('/logout', (req, res) => {
  locker.router.unlockAll(req.sessionID).then(() => {
    delete req.session.loggedin;
    delete req.session.userId;
    delete req.session.username;
    delete req.session.expires;
    res.status(200);
    res.send('Logout ok');
  });
});

module.exports = router;
