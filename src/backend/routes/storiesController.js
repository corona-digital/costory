const express = require('express');

const router = express.Router();
const { Op } = require('sequelize');
const database = require('../service/db');
const paragraphController = require('./paragraphsController');
const locker = require('./concurrentController');
const likeController = require('./likesController');

const story = database.stories;
const StoryTagAssociation = database.storiesTags;

/**
 * Get a story by id
 * @Param : :id , id of story
 */
router.get('/:id', (req, res) => {
  story.findOne({
    where: {
      id: req.params.id,
    },
    include: [
      'user',
      {
        model: database.paragraphs,
        as: 'paragraphs',
        separate: true,
        order: [['date', 'asc']],
      },
      {
        model: database.tags,
        as: 'tags',
        // This prevent the mapping of the tblStory_tblTags association table
        through: { attributes: [] },
      },
      {
        model: database.comments,
        as: 'comments',
        separate: true,
        order: [['date', 'asc']],
        include: [
          'comment',
        ],
      },
    ],
  }).then((r) => {
    if (r == null) {
      res.status(404);
      res.send(`No story with id ${req.params.id}`);
      return;
    }
    likeController.countVote(req.params.id).then((count) => {
      r.setDataValue('likes', count);
      res.send(r);
    }).catch((err) => res.send(err));
  }).catch((err) => {
    res.send(err);
  });
});
/**
 * Get all stories
 */
router.get('/', (req, res) => {
  const {
    tags,
    inputSearch,
  } = req.query;
  let optionsTags; // Options of tags
  let fullQuery; // Full query

  // If the user doesn't want to filter by tags

  if (tags === undefined) {
    optionsTags = {
      model: database.tags,
      as: 'tags',
      // This prevent the mapping of the tblStory_tblTags association table
      through: { attributes: [] },
    };
  } else {
    const tagsSplit = tags.split('-');
    const whereCondition = [];

    tagsSplit.forEach((data) => {
      const tagId = parseInt(data, 10);
      whereCondition.push(tagId);
    });

    optionsTags = {
      model: database.tags,
      as: 'tags',
      where: {
        id: {
          [Op.in]: whereCondition,
        },
      },
      // This prevent the mapping of the tblStory_tblTags association table
      through: { attributes: [] },
    };
  }
  // If the user doesn't want search by the title like

  if (inputSearch === undefined) {
    fullQuery = {
      include: [
        'user',
        {
          model: database.paragraphs,
          as: 'paragraphs',
          separate: true,
          order: [['date', 'asc']],
        },
        {
          model: database.comments,
          as: 'comments',
          separate: true,
          order: [['date', 'asc']],
        },
        {
          model: database.tags,
          as: 'tags',
          through: { attributes: [] },
        },
        optionsTags,
      ],
    };
  } else {
    fullQuery = {
      include: [
        'user',
        {
          model: database.paragraphs,
          as: 'paragraphs',
          separate: true,
          order: [['date', 'asc']],
        },
        {
          model: database.comments,
          as: 'comments',
          separate: true,
          order: [['date', 'asc']],
        },
        optionsTags,
      ],
      where: {
        title: {
          [Op.like]: `%${inputSearch}%`,
        },
      },
    };
  }
  story.findAll(fullQuery).then((r) => {
    // Tricky way to add the likes count on the story since Sequelize won't work for that :/
    const promises = [];
    for (let i = 0; i < r.length; i += 1) {
      promises.push(likeController.countVote(r[i].getDataValue('id')));
    }
    Promise.all(promises).then((results) => {
      for (let i = 0; i < r.length; i += 1) {
        r[i].setDataValue('likes', results[i]);
      }
      res.send(r);
    });
  });
});
/**
 * Create a story
 * Then add the story-tags association
 * Then add the paragraph
 * Finally, return a 201 and the id of the story
 */
router.post('/', (req, res) => {
  // Check some fields
  if (req.body.paragraph === undefined) {
    res.status(403).send('Paragraph is mandatory !');
    return;
  }
  if (req.body.paragraph.content === '') {
    res.status(403).send('Paragraph cannot be empty !');
    return;
  }

  let storyId = null;

  story.create({
    title: req.body.title,
    minParagraph: req.body.minParagraph,
    maxParagraph: req.body.maxParagraph,
    tblUserId: req.body.tblUserId,
    isDone: false,
  }).then((resp) => {
    // Add the tags into the association table
    storyId = resp.id;
    req.body.tags.forEach((tag) => {
      StoryTagAssociation.create({
        tblStoryId: storyId,
        tblTagId: tag.id,
      }).catch((error) => {
        res.status(400).send(error);
      });
    });
  }).then(() => {
    // Add the paragraph
    const payload = {
      tblStoryId: storyId,
      text: req.body.paragraph.content,
      note: 0,
      tblUserId: req.body.tblUserId,
      sessionID: req.sessionID,
    };
    paragraphController.addParagraph(payload).then(() => {
      res.status(201).send({ storyId });
    }, (reason) => {
      res.status(403).send(reason);
    });
  }).catch((error) => {
    res.status(400).send(error);
  });
});

/**
 * Get stories created by a user
 * @Params userid : user id
 */
router.get('/created/:userid', (req, res) => {
  story.findAll({
    where: {
      tblUserId: req.params.userid,
    },
    include: [
      {
        model: database.paragraphs,
        as: 'paragraphs',
        separate: true,
        order: [['date', 'asc']],
      },
      {
        model: database.tags,
        as: 'tags',
        // This prevent the mapping of the tblStory_tblTags association table
        through: { attributes: [] },
      },
    ],
  }).then((r) => res.send(r));
});

/**
 * Get the stories where a user "participated" (contributed with a paragraph)
 * @Params userid : user id
 */
router.get('/participated/:userid', (req, res) => {
  story.findAll({
    where: {
      tblUserId: {
        [Op.or]: {
          [Op.ne]: req.params.userid,
          [Op.eq]: null,
        },
      },
      '$paragraphs.tblUserId$': req.params.userid,
    },
    include: [{
      model: database.paragraphs,
      as: 'paragraphs',
      order: [['date', 'asc']],
    },
    {
      model: database.tags,
      as: 'tags',
      // This prevent the mapping of the tblStory_tblTags association table
      through: { attributes: [] },
    },
    ],
  }).then((r) => {
    console.log(r);
    res.send(r);
  });
});

/**
 * Close a story with a given :id
 * We check if the story is not locked by someone else first
 */
router.patch('/close/', (req, res) => {
  // Check if the user have access to the story
  locker.router.canEdit(req.body.storyId, req.sessionID).then((result) => {
    // If the user cannot edit the story
    if (!result) {
      res.status(401);
      res.send(result);
    } else {
      story.update(
        { isDone: 1 },
        { where: { id: req.body.storyId } },
      ).then(() => {
        res.send(true);
      }).catch((err) => {
        res.status(401);
        res.send(err);
      });
    }
  }).catch((() => {
    res.status(401);
    res.send(false);
  }));
});

module.exports = router;
