const express = require('express');

const router = express.Router();
const database = require('../service/db');

const locker = database.storiesLock;
const story = database.stories;
/**
 * The locker work with the DB
 * In the db, we have a table call 'tblLockStory' with the columns :
 * - storyId
 * - sessionId
 *
 * When a user lock a story, we check if the story is not already locked. If no, we insert a row
 * Same thing when the user unlock, we check and then delete the row
 */

/**
 * Return a promise that contains true or false, depend if the story is not already locked
 * @param _storyId
 * @param _sessionId
 * @returns {Promise<unknown>}
 */
router.canEdit = function (_storyId, _sessionId) {
  return new Promise((res, rej) => {
    // First check is the story exist
    story.findOne({
      where: {
        id: _storyId,
      },
    }).then((r) => {
      if (r === null) {
        res(false);
      } else {
        // If the story exist, check if locked
        locker.findOne({
          where: {
            storyId: _storyId,
          },
        }).then((sRes) => {
          // story is not yet in the table
          if (sRes === null) {
            res(true);
            return;
          }
          // Check if the lock is made by the same _sessionId
          res(sRes.dataValues.sessionId === _sessionId);
        }).catch((error) => {
          rej(error);
        });
      }
    });
  });
};

/**
 * Lock a story, return false if the story is already locked
 * @param storyId
 * @param sessionId
 * @returns {Promise<unknown>}
 */
router.lockStory = function (storyId, sessionId) {
  return new Promise((res, rej) => {
    router.canEdit(storyId, sessionId).then((result) => {
      if (!result) {
        rej(Error('Cannot lock this story'));
      } else {
        // Lock the story
        locker.create({
          storyId,
          sessionId,
        }).then(() => {
          res('Sucess');
        }).catch((err) => rej(Error(err)));
      }
    }).catch(((reason) => rej(Error(reason))));
  });
};

/**
 * Unlock a story, return false if the story cannot be unlock (because it's lock by another sess id)
 * @param storyId
 * @param sessionId
 * @returns {Promise<unknown>}
 */
router.unlockStory = function (storyId, sessionId) {
  return new Promise((res, rej) => {
    router.canEdit(storyId, sessionId).then((result) => {
      if (!result) {
        rej(Error('Cannot unlock this story'));
      } else {
        // Lock the story
        locker.destroy({
          where: {
            storyId,
          },
        }).then(() => {
          res('Sucess');
        }).catch((err) => rej(Error(err)));
      }
    }).catch(((reason) => rej(Error(reason))));
  });
};

/**
 * Unlock all the story locked by a session id
 * @param sessionId
 * @returns {Promise<unknown>}
 */
router.unlockAll = function (sessionId) {
  return new Promise((res, rej) => {
    // Lock the story
    locker.destroy({
      where: {
        sessionId,
      },
    }).then(() => {
      res('Sucess');
    }).catch((err) => rej(Error(err)));
  });
};

/**
 * Endpoints
 */
router.get('/canIEdit/:id', (req, res) => {
  router.canEdit(req.params.id, req.sessionID).then((result) => {
    if (!result) {
      res.status(401);
    }
    res.send(result);
  }).catch((() => {
    res.status(401);
    res.send(false);
  }));
});
/**
 * Story lock endpoint
 * @Return 403 if forbidden
 */
router.get('/lock/:id', (req, res) => {
  router.lockStory(req.params.id, req.sessionID)
    .then(() => res.send(true))
    .catch(() => {
      res.status(403);
      res.send(false);
    });
});
/**
 * Unlock story endpoint
 * @Return 403 if forbidden
 */
router.get('/unlock/:id', (req, res) => {
  router.unlockStory(req.params.id, req.sessionID)
    .then(() => res.send(true))
    .catch(() => {
      res.status(403);
      res.send(false);
    });
});

/**
 * Unlock all stories locked by a given session-id
 */
router.get('/unlockAll/', (req, res) => {
  router.unlockAll(req.sessionID)
    .then(() => res.send(true))
    .catch(() => {
      res.status(403);
      res.send(false);
    });
});

module.exports = {
  router,
};
