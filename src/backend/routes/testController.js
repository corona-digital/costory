const express = require('express');

const router = express.Router();
const database = require('../service/db');

/**
 * Clear the database, wipe all the stories
 * Used for testing only !
 */
router.put('/resetDB', (req, res) => {
  database.stories.destroy({
    where: {},
  }).then(() => {
    database.stories.destroy({
      where: {},
    }).then(() => res.sendStatus(200))
      .catch((err) => res.status(500).send(err));
  })
    .catch((err) => res.status(500).send(err));
});

module.exports = router;
