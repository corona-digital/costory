const express = require('express');
const database = require('../service/db');

const router = express.Router();

const paragraph = database.paragraphs;
const story = database.stories;
const locker = require('./concurrentController');

/**
 * Check if a story can have more paragraphs, given his limit (maxParagraphs)
 * @param storyId
 * @returns {Promise<true | false>}
 */
router.checkParagraphLimit = async function (storyId) {
  return new Promise((res, rej) => {
    story.findOne({
      where: {
        id: storyId,
      },
      attributes: ['maxParagraph'],
    }).then((sRes) => {
      const { maxParagraph } = sRes.dataValues;
      paragraph.findAndCountAll({
        where: {
          tblStoryId: storyId,
        },
      }).then((pRes) => {
        res(maxParagraph - (pRes.count));
      });
    }).catch((error) => {
      rej(error);
    });
  });
};

/**
 * Add a paragraph in a story
 * Check if the user can edit the story and if the paragraph limit is not reached
 * @param req
 * @returns {Promise<unknown>}
 */
router.addParagraph = function (req) {
  return new Promise((res, rej) => {
    if (req.text === '') {
      rej(Error('Content is empty'));
    }
    // Check if the story in not locked by another user
    locker.router.canEdit(req.tblStoryId, req.sessionID).then((canEdit) => {
      if (!canEdit) {
        rej(Error('This story is locked'));
      } else {
        // Check max paragraphs limit
        router.checkParagraphLimit(req.tblStoryId).then((result) => {
          if (result === 0) {
            rej(Error('Paragraph limit reached'));
            return;
          }
          paragraph.create({
            tblStoryId: req.tblStoryId,
            text: req.text,
            tblUserId: req.tblUserId,
            date: new Date(),
          }).then((resp) => {
            if (result === 1) {
              story.findOne({
                where: {
                  id: req.tblStoryId,
                },
              }).then((storyFound) => {
                if (storyFound) {
                  storyFound.update({
                    isDone: true,
                  }).then(() => {
                    res(resp);
                  }).catch((error) => {
                    rej(error);
                  });
                }
              }).catch((error) => {
                rej(error);
              });
            } else {
              res(resp);
            }
          }).catch((error) => {
            rej(Error(error));
          });
        });
      }
    });
  });
};

/**
 * Get a paragraph by id
 */
router.get('/:id', (req, res) => {
  paragraph.findOne({
    where: {
      id: req.params.id,
    },
    include: ['user'],
  }).then((r) => {
    if (r == null) {
      res.status(404);
      res.send(`No paragraph with id ${req.params.id}`);
      return;
    }
    res.send(r);
  });
});

/**
 * Get all paragraphs
 */
router.get('/', (req, res) => {
  paragraph.findAll({
    include: ['user'],
  }).then((r) => res.send(r));
});

/**
 * Add a paragraph
 */
router.post('/', (req, res) => {
  const payload = {
    tblStoryId: req.body.tblStoryId,
    text: req.body.text,
    tblUserId: req.body.tblUserId,
    sessionID: req.sessionID,
  };

  router.addParagraph(payload).then((result) => {
    res.status(201).send(result);
  }).catch((reason) => {
    res.status(403).send(reason);
  });
});

module.exports = router;
