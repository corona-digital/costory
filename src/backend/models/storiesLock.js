/**
 * Definition of the StoriesLock model for the sequelize database
 * Those entity are ment to check if a story is currently written by a user
 * wich is defined by his session-id (since a user can be anonymous)
 * @param sequelize
 * @param DataTypes
 * @returns {void|*|ModelCtor<Model>}
 */
module.exports = (sequelize, DataTypes) => {
  const StoriesLock = sequelize.define('tblStoryLock', {
    // Model attributes are defined here
    storyId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    sessionId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {
    // Other model options go here
    tableName: 'tblStoryLock',
  });
  return StoriesLock;
};
