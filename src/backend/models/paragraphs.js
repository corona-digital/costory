/**
 * Definition of the Paragraphs model for the sequelize database
 * @param sequelize
 * @param DataTypes
 * @returns {void|*|ModelCtor<Model>}
 */
module.exports = (sequelize, DataTypes) => {
  const Paragraphs = sequelize.define('tblParagraph', {
    // Model attributes are defined here
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    tblStoryId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      foreignKey: true,
    },
    text: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    date: {
      type: DataTypes.DATE,
      allowNull: true, // do not worry, it set a default value in mysql ;)
    },
    tblUserId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      foreignKey: true,
    },
  }, {
    // Other model options go here
    tableName: 'tblParagraph',
  });
  return Paragraphs;
};
