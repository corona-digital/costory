/**
 * Definition of the Stories model for the sequelize database
 * @param sequelize
 * @param DataTypes
 * @returns {void|*|ModelCtor<Model>}
 */
module.exports = (sequelize, DataTypes) => {
  const Stories = sequelize.define('tblStory', {
    // Model attributes are defined here
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    minParagraph: {
      type: DataTypes.INTEGER,
      allowNull: false,
      // allowNull defaults to true
    },
    maxParagraph: {
      type: DataTypes.INTEGER,
      allowNull: false,
      // allowNull defaults to true
    },
    isDone: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      // allowNull defaults to true
    },
    tblUserId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      foreignKey: true,
    },
  }, {
    // Other model options go here
    tableName: 'tblStory',
  });
  return Stories;
};
