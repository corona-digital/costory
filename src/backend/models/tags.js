/**
 * Definition of the Tags model for the sequelize database
 * @param sequelize
 * @param DataTypes
 * @returns {void|*|ModelCtor<Model>}
 */
module.exports = (sequelize, DataTypes) => {
  const Tags = sequelize.define('tblTag', {
    // Model attributes are defined here
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {
    // Other model options go here
    tableName: 'tblTag',
  });
  return Tags;
};
