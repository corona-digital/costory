/**
 * Definition of the Likes model for the sequelize database
 * @param sequelize
 * @param DataTypes
 * @returns {void|*|ModelCtor<Model>}
 */
module.exports = (sequelize, DataTypes) => {
  const Likes = sequelize.define('tblUser_like_tblStory', {
    // Model attributes are defined here
    tblStoryId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      foreignKey: true,
    },
    tblUserId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      foreignKey: true,
    },
    value: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    // Other model options go here
    tableName: 'tblUser_like_tblStory',
  });
  return Likes;
};
