/**
 * Definition of the StoriesTags model for the sequelize database
 * Each stories must have at least one tag.
 * @param sequelize
 * @param DataTypes
 * @returns {void|*|ModelCtor<Model>}
 */
module.exports = (sequelize, DataTypes) => {
  const StoriesTags = sequelize.define('tblStory_tblTag', {
    // Model attributes are defined here
    tblStoryId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    tblTagId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    // Other model options go here
    tableName: 'tblStory_tblTag',
  });
  return StoriesTags;
};
