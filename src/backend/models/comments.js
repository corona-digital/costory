/**
 * Definition of the Comments model for the sequelize database
 * Comments can be a child of a story or other comments
 * @param sequelize
 * @param DataTypes
 * @returns {void|*|ModelCtor<Model>}
 */
module.exports = (sequelize, DataTypes) => {
  const Comments = sequelize.define('tblComment', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    tblStoryId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      foreignKey: true,
    },
    text: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    date: {
      type: DataTypes.DATE,
      allowNull: true, // do not worry, it set a default value in mysql ;)
    },
    tblUserId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      foreignKey: true,
    },
    tblCommentId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      foreignKey: true,
    },
  }, {
    // Other model options go here
    tableName: 'tblComment',
  });
  return Comments;
};
