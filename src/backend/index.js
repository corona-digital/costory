// imports
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv').config(); // for .env files
const database = require('./service/db');

/**
 * Cors policy : list of allowed adress for the API requests
 */
const allowedOrigins = [
  'https://costory.ch',
  'http://localhost:8080',
  'http://localhost:80',
  'http://localhost',
  'http://localhost:4000',
  'http://172.28.0.4',
  'https://www.costory.ch',
];

/**
 * Express setup
 */
const app = express();
app.use(bodyParser.json());

/**
 * Cors policy
 */
if (process.env.NODE_ENV === 'development') {
  app.use(cors());
}

if (process.env.NODE_ENV === 'production') {
  app.use(cors(
    {
      origin: (origin, callback) => {
        if (allowedOrigins.indexOf(origin) === -1) {
          const msg = `${'The CORS policy for this site does not '
                + 'allow access from the specified Origin ('}${origin})`;
          return callback(new Error(
            msg,
          ), false);
        }

        return callback(null, true);
      },
    },
  ));
}

// Setup session with DB
database.setupSession(app);

// Routing
const user = require('./routes/usersController');
const story = require('./routes/storiesController');
const paragraph = require('./routes/paragraphsController');
const comment = require('./routes/commentsController');
const tag = require('./routes/tagsController');
const login = require('./routes/authenticationController');
const concurrent = require('./routes/concurrentController');
const likes = require('./routes/likesController');
const test = require('./routes/testController');

app.use('/api/users', user);
app.use('/api/stories', story);
app.use('/api/paragraphs', paragraph);
app.use('/api/comments', comment);
app.use('/api/tags', tag);
app.use('/api/auth', login);
app.use('/api/auth', concurrent.router);
app.use('/api/likes', likes);
app.use('/api/test', test);

/**
 * Starting server
 */
const port = process.env.PORT || 4000;

app.server = app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`listening on ${port}`);
});

module.exports = app;
