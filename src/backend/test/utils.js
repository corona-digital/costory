/* eslint-disable func-names */
/* eslint-disable no-plusplus */

const utils = {};

utils.makeString = function (length) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

utils.generateUser = function () {
  return {
    username: utils.makeString(5),
    email: `${utils.makeString(5)}@${utils.makeString(5)}`,
    password: 'superpassword',
  };
};

module.exports = utils;
