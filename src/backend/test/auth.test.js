/* eslint-disable no-undef */
require('mysql2/node_modules/iconv-lite').encodingExists('foo');

const request = require('supertest'); // supertest is a framework that allows to easily test web apis
const app = require('../index');
const db = require('../service/db');
const utils = require('./utils');

afterAll(async (done) => {
  await db.sequelize.close();
  await app.server.close();
  done();
});

const user = utils.generateUser();

describe('Authentification API', () => {
  it('should add an user', async () => {
    const query = {
      username: user.username,
      email: user.email,
      password: user.password,
      passwordConfirm: user.password,
    };
    const res = await request(app).post('/api/users/').send(query);
    expect(res.statusCode).toEqual(201);
    expect(res.body.id).toBeDefined();
    user.id = res.body.id;
    expect(res.body.username).toBeDefined();
    expect(res.body.email).toBeDefined();
    expect(res.body.password).toBeDefined();
    expect(res.body.isAdmin).toBeDefined();
  });

  it('should get the user', async () => {
    const res = await request(app).get(`/api/users/${user.id}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body.id).toEqual(user.id);
    expect(res.body.username).toEqual(user.username);
    expect(res.body.email).toEqual(user.email);
  });

  it('should login', async () => {
    const query = {
      username: user.username,
      password: user.password,
    };
    const res = await request(app).post('/api/auth/login').send(query);
    expect(res.statusCode).toEqual(200);
    expect(res.body.id).toBeDefined();
    expect(res.body.username).toBeDefined();
    expect(res.body.email).toBeDefined();
  });

  it('should logout', async () => {
    const res = await request(app).get('/api/auth/logout');
    expect(res.statusCode).toEqual(200);
  });

  it('should show all users', async () => {
    const res = await request(app).get('/api/users');
    expect(res.statusCode).toEqual(200);
  });
});
