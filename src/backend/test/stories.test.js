/* eslint-disable no-restricted-syntax */
/* eslint-disable no-undef */
require('mysql2/node_modules/iconv-lite').encodingExists('foo');

const request = require('supertest'); // supertest is a framework that allows to easily test web apis
const app = require('../index');
const db = require('../service/db');
const utils = require('./utils');

jest.useFakeTimers();

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

afterAll(async (done) => {
  await db.sequelize.close();
  await app.server.close();
  done();
});

const user = utils.generateUser();
const user3 = utils.generateUser();
const user4 = utils.generateUser();
const story = {
  title: 'une histoire',
  paragraph: 'un ptit paragraphe',
};
const paragraph = 'un autre ptit paragraphe';
const comment = 'un ptit commentaire';

describe('Stories API', () => {
  it('should add a story', async () => {
    // generate a writer
    let query = {
      username: user.username,
      email: user.email,
      password: user.password,
      passwordConfirm: user.password,
    };

    let res = await request(app).post('/api/users').send(query);
    user.id = res.body.id;

    // add the story
    query = {
      title: story.title,
      minParagraph: 1,
      maxParagraph: 2,
      tblUserId: user.id,
      tags: [
        {
          id: 1,
        },
        {
          id: 2,
        },
      ],
      paragraph:
        {
          content: story.paragraph,
        },
    };
    res = await request(app).post('/api/stories').send(query);
    expect(res.statusCode).toEqual(201);
    expect(res.body.storyId).toBeDefined();
    story.id = res.body.storyId;
  });

  it('should add likes', async () => {
    // generate users
    let query = {
      username: user4.username,
      email: user4.email,
      password: user4.password,
      passwordConfirm: user4.password,
    };
    let res = await request(app).post('/api/users').send(query);
    user4.id = res.body.id;

    query = {
      username: user3.username,
      email: user3.email,
      password: user3.password,
      passwordConfirm: user3.password,
    };
    res = await request(app).post('/api/users').send(query);
    user3.id = res.body.id;

    // send likes
    query = {
      tblUserId: user.id,
      tblStoryId: story.id,
      value: '1',
    };
    res = await request(app).post('/api/likes').send(query);
    expect(res.statusCode).toEqual(200);

    query = {
      tblUserId: user4.id,
      tblStoryId: story.id,
      value: '1',
    };
    res = await request(app).post('/api/likes').send(query);
    expect(res.statusCode).toEqual(200);

    query = {
      tblUserId: user3.id,
      tblStoryId: story.id,
      value: '-1',
    };
    res = await request(app).post('/api/likes').send(query);
    expect(res.statusCode).toEqual(200);
  });

  it('should add a paragraph', async () => {
    const query = {
      tblStoryId: story.id,
      text: paragraph,
    };
    res = await request(app).post('/api/paragraphs').send(query);
    expect(res.statusCode).toEqual(201);
    expect(res.body.tblStoryId).toEqual(story.id);
    expect(res.body.id).toBeDefined();
    expect(res.body.text).toEqual(paragraph);
    expect(res.body.date).toBeDefined();
    paragraph.id = res.body.id;
  });

  it('should add a comment', async () => {
    // add the story
    const query = {
      tblStoryId: story.id,
      text: comment,
      tblUserId: user.id,
    };
    res = await request(app).post('/api/comments').send(query);
    expect(res.statusCode).toEqual(201);
    expect(res.body.tblStoryId).toEqual(story.id);
    expect(res.body.id).toBeDefined();
    expect(res.body.text).toEqual(comment);
    expect(res.body.date).toBeDefined();
    comment.id = res.body.id;
  });

  it('should show all stories filtered by tags', async () => {
    // add the story

    const listOfTags = [4, 5];
    const tagsChosen = [];
    let tagsUrl = '';

    for (let i = 0; i < listOfTags.length; i += 1) {
      const idTag = listOfTags[i];

      if (i > 0) {
        tagsUrl += '-';
      }

      tagsChosen.push({
        id: idTag,
      });

      tagsUrl += idTag;
    }

    query = {
      title: story.title,
      minParagraph: 1,
      maxParagraph: 2,
      tblUserId: user.id,
      tags: tagsChosen,
      paragraph:
        {
          content: story.paragraph,
        },
    };

    await request(app).post('/api/stories').send(query);

    query = {
      title: story.title,
      minParagraph: 1,
      maxParagraph: 2,
      tblUserId: user.id,
      tags: tagsChosen,
      paragraph:
        {
          content: story.paragraph,
        },
    };

    await request(app).post('/api/stories').send(query);

    const res = await request(app).get(`/api/stories?tags=${tagsUrl}`);

    expect(res.body.length).toBeGreaterThanOrEqual(2);
    expect(res.statusCode).toEqual(200);
  });

  it('should get the story', async () => {
    const res = await request(app).get(`/api/stories/${story.id}`);
    expect(res.body.comments[0].text).toEqual(comment);
    expect(res.body.paragraphs[0].text).toEqual(story.paragraph);
    expect(res.body.paragraphs[1].text).toEqual(paragraph);
    expect(res.body.tags).toBeDefined();
    expect(res.body.likes).toEqual('1');
    expect(res.statusCode).toEqual(200);
  });

  it('should show all stories', async () => {
    const res = await request(app).get('/api/stories');
    expect(res.statusCode).toEqual(200);
  });
});

describe('Get a story related to a user', () => {
  const user1 = utils.generateUser();
  const user2 = utils.generateUser();

  const story1U1 = {
    title: 'une histoire 1',
    minParagraph: 1,
    maxParagraph: 10,
    tags: [
      {
        id: 1,
      },
      {
        id: 2,
      },
    ],
    paragraph:
      {
        content: 'un ptit paragraphe 1',
      },
  };

  const story2U1 = {
    title: 'une histoire 2',
    minParagraph: 1,
    maxParagraph: 10,
    tags: [
      {
        id: 1,
      },
      {
        id: 2,
      },
    ],
    paragraph:
      {
        content: 'un ptit paragraphe 2',
      },
  };

  const story3U2 = {
    title: 'une histoire 3',
    minParagraph: 1,
    maxParagraph: 10,
    tags: [
      {
        id: 1,
      },
      {
        id: 2,
      },
    ],
    paragraph:
      {
        content: 'un ptit paragraphe 3',
      },
  };

  const anonymousStory = {
    title: 'Anonymous Story',
    minParagraph: 1,
    maxParagraph: 10,
    tags: [
      {
        id: 1,
      },
      {
        id: 2,
      },
    ],
    paragraph:
      {
        content: 'This is a anonymous story',
      },
  };

  beforeAll(async () => {
    // Create user
    const query1 = {
      username: user1.username,
      email: user1.email,
      password: user1.password,
      passwordConfirm: user1.password,
    };
    const res1 = await request(app).post('/api/users').send(query1);
    user1.id = res1.body.id;

    const query2 = {
      username: user2.username,
      email: user2.email,
      password: user2.password,
      passwordConfirm: user2.password,
    };

    const res2 = await request(app).post('/api/users').send(query2);
    user2.id = res2.body.id;

    // Create story

    // By user 1
    const queryS1 = {
      title: story1U1.title,
      minParagraph: story1U1.minParagraph,
      maxParagraph: story1U1.maxParagraph,
      tblUserId: user1.id,
      tags: [
        {
          id: story1U1.tags[0].id,
        },
        {
          id: story1U1.tags[1].id,
        },
      ],
      paragraph:
        {
          content: story1U1.paragraph.content,
        },
    };

    const resStory1 = await request(app).post('/api/stories').send(queryS1);
    story1U1.id = resStory1.body.storyId;

    const queryS2 = {
      title: story2U1.title,
      minParagraph: story2U1.minParagraph,
      maxParagraph: story2U1.maxParagraph,
      tblUserId: user1.id,
      tags: [
        {
          id: story2U1.tags[0].id,
        },
        {
          id: story2U1.tags[1].id,
        },
      ],
      paragraph:
        {
          content: story2U1.paragraph.content,
        },
    };

    const resStory2 = await request(app).post('/api/stories').send(queryS2);
    story2U1.id = resStory2.body.storyId;

    // By user 2
    const queryS3 = {
      title: story3U2.title,
      minParagraph: story3U2.minParagraph,
      maxParagraph: story3U2.maxParagraph,
      tblUserId: user2.id,
      tags: [
        {
          id: story3U2.tags[0].id,
        },
        {
          id: story3U2.tags[1].id,
        },
      ],
      paragraph:
        {
          content: story3U2.paragraph.content,
        },
    };

    const resStory3 = await request(app).post('/api/stories').send(queryS3);
    story3U2.id = resStory3.body.storyId;

    const queryS4 = {
      title: anonymousStory.title,
      minParagraph: anonymousStory.minParagraph,
      maxParagraph: anonymousStory.maxParagraph,
      tags: [
        {
          id: story1U1.tags[0].id,
        },
        {
          id: story1U1.tags[1].id,
        },
      ],
      paragraph:
        {
          content: anonymousStory.paragraph.content,
        },
    };

    const resAnoStory1 = await request(app).post('/api/stories').send(queryS4);
    anonymousStory.id = resAnoStory1.body.storyId;

    // Add a paragraph to story 1
    const paragraph1S1 = {
      tblStoryId: story1U1.id,
      text: 'un autre ptit paragraphe 1',
      tblUserId: null,
    };

    const paragraph2S1U2 = {
      tblStoryId: story1U1.id,
      text: 'un autre ptit paragraphe 2 par user 2',
      tblUserId: user2.id,
    };

    await request(app).post('/api/paragraphs').send(paragraph1S1);

    sleep(2000);

    jest.advanceTimersByTime(2000);

    await request(app).post('/api/paragraphs').send(paragraph2S1U2);
  });

  it('should return stories create by the user', async () => {
    const resUser = await request(app).get(`/api/stories/created/${user1.id}`);
    expect(resUser.body.length).toEqual(2);
    expect(resUser.body[0].title).toEqual(story1U1.title);
    expect(resUser.body[1].title).toEqual(story2U1.title);
  });

  it('should return stories participated by the user', async () => {
    const resUser = await request(app).get(`/api/stories/participated/${user2.id}`);
    console.log(resUser.body);
    expect(resUser.body.length).toEqual(1);
    expect(resUser.body[0].title).toEqual(story1U1.title);
  });

  it('should return stories participated by the user even if the story owner is anonymous', async () => {
    const addParaQuery = {
      tblStoryId: anonymousStory.id,
      text: 'Encore un autre paragraphe',
      tblUserId: user2.id,
    };

    await request(app).post('/api/paragraphs').send(addParaQuery);

    const resUser = await request(app).get(`/api/stories/participated/${user2.id}`);
    expect(resUser.statusCode).toEqual(200);
    expect(resUser.body[1].title).toContain(anonymousStory.title);
  });

  it('should return only one story if the user writes paragraphs in the same story', async () => {
    const paragraph3 = 'un autre ptit paragraphe 3';

    const queryParagraphStory3 = {
      tblStoryId: anonymousStory.id,
      text: paragraph3,
      tblUserId: user2.id,
    };

    await request(app).post('/api/paragraphs').send(queryParagraphStory3);

    const resUser = await request(app).get(`/api/stories/participated/${user2.id}`);
    expect(resUser.statusCode).toEqual(200);
    expect(resUser.body.length).toEqual(2);
  });
});
