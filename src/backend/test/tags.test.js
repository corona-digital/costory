/* eslint-disable quote-props */
/* eslint-disable no-undef */
require('mysql2/node_modules/iconv-lite').encodingExists('foo');

const request = require('supertest'); // supertest is a framework that allows to easily test web apis
const app = require('../index');
const db = require('../service/db');

afterAll(async (done) => {
  await db.sequelize.close();
  await app.server.close();
  done();
});

describe('Tags API', () => {
  it('should show all tags', async () => {
    const res = await request(app).get('/api/tags');
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual([
      {
        'id': 2,
        'name': 'Adventure',
      },
      {
        'id': 14,
        'name': 'Art',
      },
      {
        'id': 24,
        'name': 'Children',
      },
      {
        'id': 4,
        'name': 'Contemporary',
      },
      {
        'id': 13,
        'name': 'Cooking',
      },
      {
        'id': 16,
        'name': 'Development',
      },
      {
        'id': 5,
        'name': 'Dystopian',
      },
      {
        'id': 22,
        'name': 'Families & Relationships',
      },
      {
        'id': 1,
        'name': 'Fantasy',
      },
      {
        'id': 21,
        'name': 'Guide / How-to',
      },
      {
        'id': 18,
        'name': 'Health',
      },
      {
        'id': 10,
        'name': 'Historical Fiction',
      },
      {
        'id': 19,
        'name': 'History',
      },
      {
        'id': 7,
        'name': 'Horror',
      },
      {
        'id': 23,
        'name': 'Humor',
      },
      {
        'id': 12,
        'name': 'Memoir',
      },
      {
        'id': 17,
        'name': 'Motivational',
      },
      {
        'id': 6,
        'name': 'Mystery',
      },
      {
        'id': 9,
        'name': 'Paranormal',
      },
      {
        'id': 3,
        'name': 'Romance',
      },
      {
        'id': 11,
        'name': 'Science Fiction',
      },
      {
        'id': 15,
        'name': 'Self-help / Personal',
      },
      {
        'id': 8,
        'name': 'Thriller',
      },
      {
        'id': 20,
        'name': 'Travel',
      },
    ]);
  });
});
