# Start database

- `run-database.sh`

## Connecting to db

1. Connect to the mysql container
`docker exec -it costory_db_container mysql -u dbDevcostory -p`
2. Enter the root password : `devpass`
3. Enter the app's database by typing : `mysql> USE costory`
4. Now you can display all tables with the command : `show tables;`

# Start App

`node index.js`

# Run tests in dev mode

- Change .env file to "localhost:6603"
- `run-database.sh`
- `npm run test`

# Run tests with docker containers

`./run-integration-tests.sh`