const Sequelize = require('sequelize');
const expressSession = require('express-session');
const SessionStore = require('express-session-sequelize')(expressSession.Store);
// const cookieParser = require('cookie-parser');

/**
 *  Database connexion
 */
const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  dialect: 'mysql',
  define: {
    timestamps: false,
  },
});

// Sessions store
const sequelizeSessionStore = new SessionStore({
  db: sequelize,
});

// Connect all the models/tables in the database to a db object,
// so everything is accessible via one object

const db = {};

db.Sequelize = Sequelize;

// Models/tables
db.users = require('../models/users.js')(sequelize, Sequelize);
db.stories = require('../models/stories.js')(sequelize, Sequelize);
db.paragraphs = require('../models/paragraphs.js')(sequelize, Sequelize);
db.comments = require('../models/comments.js')(sequelize, Sequelize);
db.tags = require('../models/tags.js')(sequelize, Sequelize);
db.storiesTags = require('../models/storiesTags.js')(sequelize, Sequelize);
db.storiesLock = require('../models/storiesLock.js')(sequelize, Sequelize);
db.likes = require('../models/likes.js')(sequelize, Sequelize);

/**
 * Sequelize relations
 * We define here all the relations between our models
 */
db.users.hasMany(db.stories, {
  as: 'stories',
});
db.users.hasMany(db.paragraphs, {
  as: 'paragraphs',
});
db.stories.hasMany(db.paragraphs, {
  as: 'paragraphs',
});
db.stories.hasMany(db.comments, {
  as: 'comments',
});

db.stories.belongsTo(db.users, {
  foreignKey: 'tblUserId',
  as: 'user',
});

db.stories.belongsToMany(db.tags, {
  through: 'tblStory_tblTag',
  foreignKey: 'tblStoryId',
  as: 'searchedTags',
});

db.stories.belongsToMany(db.tags, {
  through: 'tblStory_tblTag',
  foreignKey: 'tblStoryId',
  as: 'tags',
});
db.tags.belongsToMany(db.stories, {
  through: 'tblStory_tblTag',
  foreignKey: 'tblTagId',
  as: 'stories',
});

db.stories.belongsToMany(db.users, {
  through: 'tblUser_like_tblStory',
  foreignKey: 'tblStoryId',
  as: 'usersLike',
});
db.users.belongsToMany(db.stories, {
  through: 'tblUser_like_tblStory',
  foreignKey: 'tblUserId',
  as: 'storiesLike',
});

db.paragraphs.belongsTo(db.users, {
  foreignKey: 'tblUserId',
  as: 'user',
});
db.paragraphs.belongsTo(db.stories, {
  foreignKey: 'tblStoryId',
  as: 'story',
});

db.comments.belongsTo(db.users, {
  foreignKey: 'tblUserId',
  as: 'user',
});
db.comments.belongsTo(db.stories, {
  foreignKey: 'tblStoryId',
  as: 'story',
});
db.comments.hasMany(db.comments, {
  foreignKey: 'tblCommentId',
  as: 'comment',
});

// Finalize the session management setup with db
db.setupSession = function setupSession(app) {
  // app.use(cookieParser); // Bloque complétement les requetes entrantes :/ pourquoi ?
  app.use(expressSession({
    name: 'sid',
    secret: 'TOPSECRET', // TODO changer ça dans le fichier .env ?
    resave: false,
    saveUninitialized: true,
    store: sequelizeSessionStore,
    cookie: {
      secure: false, // TODO : for dev only !
      maxAge: 1000 * 60 * 60 * 24, // 1 day
    },
  }));
};

db.sequelize = sequelize;

module.exports = db;
